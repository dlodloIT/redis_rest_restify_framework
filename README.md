# redis_rest_restify_framework

#### 项目介绍
整合redis，restify提供rest服务的一个node js代码框架

特点：在开发时支持自动更新脚本检测、检测后，websocket自动更新当前页面，实现无缝开发+API结果集成测试

#### 软件架构
1. 重新构建HTTP Server端的封装，自动检测并自动映射文件夹中的文件路由及脚本方法。
2. 提供redis基础封装类，提升开发效率


#### 安装教程

1. vs2017(Visual Studio 2017)
2. nodejs
3. npm

#### 使用说明

1. 使用本模块时，建议直接使用相对路径设置package.json引用，实现开发时不再需要依赖npm安装restify


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)