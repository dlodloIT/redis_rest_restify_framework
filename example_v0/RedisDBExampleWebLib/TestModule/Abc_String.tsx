import { PureComponent } from "react";
import TesterManager, { TestControl } from "./TesterManager";
import * as React from "react";
import Rest from "../Rest";
import CommonResult from "../CommonResult";

export default class abc_string extends TestControl {
    static Register() {
        TesterManager.RegisterControl("abc", "string", abc_string);
    }

    constructor(props) {
        super("string", props);

        this.$.string.read().done((data) => {
            this.ReadViewControl.Success(data);
        }).fail((error) => {
            this.ReadViewControl.Fail(error);
        })

        this.$.string.read(1).done((data) => {
            this.ReadByIDViewControl.Success(data);
        }).fail((error) => {
            this.ReadByIDViewControl.Fail(error);
        })

        this.$.string.destroy(1).done((data) => {
            if (data === true) {
                this.DelByIDViewControl.Success("true");
            } else {
                this.DelByIDViewControl.Fail("删除失败");
            }
        }).fail((error) => {
            this.DelByIDViewControl.Fail(error);
        })

        this.$.string.update(1, { value: new Date().toLocaleTimeString() }).done((data) => {
            this.UpdateByIDViewControl.Success(data?"true":"false");
        }).fail((error) => {
            this.UpdateByIDViewControl.Fail(error);
        })
    }

    ReadViewControl: CommonResult;
    ReadByIDViewControl: CommonResult;
    DelByIDViewControl: CommonResult;
    UpdateByIDViewControl: CommonResult;
    render() {
        let a = <div>
        </div>;
        return <div>
            <CommonResult RequestInfo={this.ReadRequestInfo} ref={(el) => this.ReadViewControl = el} Act="read" />
            <CommonResult RequestInfo={this.ReadRequestInfo } ref={(el) => this.ReadByIDViewControl = el} Act="read(1)" />
            <CommonResult RequestInfo={this.DestroyRequestInfo} ref={(el) => this.DelByIDViewControl = el} Act="del(1)" />
            <CommonResult RequestInfo={this.UpdateRequestInfo} ref={(el) => this.UpdateByIDViewControl = el} Act="update(1)" />
        </div>;
    }
}

