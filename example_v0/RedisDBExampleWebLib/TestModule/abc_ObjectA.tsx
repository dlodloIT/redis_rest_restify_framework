import { PureComponent } from "react";
import TesterManager, { TestControl } from "./TesterManager";
import * as React from "react";
import Rest from "../Rest";
import CommonResult from "../CommonResult";

export default class abc_ObjectA extends TestControl {
    static Register() {
        TesterManager.RegisterControl("abc", "ObjectA", abc_ObjectA);
    }
    constructor(props) {
        super("ObjectA", props);
        
        let read = () => {
            this.$.ObjectA.read().done((data: any[]) => {
                this.crRead.Success(JSON.stringify(data));
            }).fail((error) => {
                this.crRead.Fail(error);
            })
        }

        let destroyLast = (lastKey: number) => {
            this.$.ObjectA.destroy(lastKey).done((result: boolean) => {

            }).fail((error) => {

            })
        }

        this.$.ObjectA.create({ name: "测试项" }).done((createItem: { id: number }) => {
            this.crCreate.Success(JSON.stringify(createItem));

            destroyLast(createItem.id);

            read();
        }).fail((error) => {
            this.crCreate.Fail(error);
        })

        //this.$.ObjectA.create
    }
    crRead: CommonResult;
    crCreate: CommonResult;
    render() {
        return <div>
            <CommonResult RequestInfo={this.ReadRequestInfo} ref={(el) => this.crRead = el} Act="read" />
            <CommonResult RequestInfo={this.CreateRequestInfo} ref={(el) => this.crCreate = el} Act="create({ name:'测试项'})" />
        </div>;
    }
}
