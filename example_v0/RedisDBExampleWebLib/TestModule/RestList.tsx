import React = require('react');
import { MouseEvent, ChangeEvent, Component, PureComponent } from 'react';
import Config from '../Config';
import Rest from '../Rest';
import TesterManager, { TestControl } from './TesterManager';
import abc_string from './abc_string';
import abc_ObjectA from './abc_ObjectA';


interface RestResourceFullInfo {
    GroupName: string,
    ObjectName: string,
    GroupDescription: string,
    ObjectDescription: string,
    GlobalIndex: number,
    RootUrl: string,
}
interface GroupInfo {
    Name: string,
    Description: string,
}
interface ObjectInfo {
    Name: string,
    Description: string,
}

export default class TestModule_RestList extends PureComponent<
    {}, {}>{
    groupArray: GroupInfo[] = [];
    groupLoadedCount = 0;
    objectGlobalIndex = 0;
    restServices: RestResourceFullInfo[] = [];
    restServiceTree: { [index: string]: RestResourceFullInfo[] } = {}
    constructor(props) {
        super(props);
        this.GetAllRestServices();
    }

    GetAllRestServices() {
        let client = new Rest(Config.TEST_ROOT_URI);

        client.Uri.add(Config.TEST_MetaObjectName);
        client.$[Config.TEST_MetaObjectName].read().done((data: GroupInfo[]) => {
            this.groupArray = data;
            for (let i in this.groupArray) {
                this.LoadObjects(this.groupArray[i]);
            }
        });
    }
    LoadObjects(group) {
        let groupClient = new Rest(Config.TEST_ROOT_URI + group.Name + "/");
        groupClient.Uri.add(Config.TEST_MetaObjectName);

        let objectArray: ObjectInfo[] = [];
        let request = groupClient.$[Config.TEST_MetaObjectName].read();
        request.done((data: ObjectInfo[]) => {
            objectArray = data;
            var objectInfoArray = [];
            for (var i in objectArray) {
                var objectInfo = objectArray[i];
                this.objectGlobalIndex++;
                var objectWithGroupInfo = {
                    RootUrl: Config.TEST_ROOT_URI,
                    GroupName: group.Name, ObjectName: objectInfo.Name, GlobalIndex: this.objectGlobalIndex,
                    GroupDescription: group.Description,
                    ObjectDescription: objectInfo.Description
                };
                objectInfoArray.push(objectWithGroupInfo);
                this.restServices.push(objectWithGroupInfo);
            }
            this.restServiceTree[group.Name] = objectInfoArray;
            this.groupLoadedCount++;
            if (this.groupLoadedCount == this.groupArray.length) {
                this.RefreshUI();
            }
        });
        request.fail(function () {
            this.groupLoadedCount++;
            if (this.groupLoadedCount == this.groupArray.length) {
                this.RefreshUI();
            }
        })
    }
    RefreshUI() {
        (this.refs.list as RestResourceList).Change(this.restServices);
    }

    render() {
        //let control = TesterManager.GetNewControl('abc', 'string');
        //
        return <div>
            <h1>REST资源对象清单与测试结果</h1>
            <div>
                <RestResourceList ref="list" />
            </div>
        </div>;
    }
}

class RestResourceList extends PureComponent<{}
    , { data: RestResourceFullInfo[] }>{
    constructor(props: { data: RestResourceFullInfo[] }) {
        super(props);
        this.state = { data: [] }
    }
    Change(listData: RestResourceFullInfo[]) {
        this.setState({ data: listData });
    }
    render() {
        return this.state.data.map((value, index) => <div key={value.GlobalIndex} id={"Object-" + value.GlobalIndex}>
            <fieldset>
                <legend>{value.GlobalIndex} : <a href={value.RootUrl + value.GroupName + "/" + value.ObjectName}>
                    [GET]{value.RootUrl}{value.GroupName}/{value.ObjectName}</a></legend>
                <div>{value.GroupDescription}<br />{value.ObjectDescription}</div>
                <div>自动化测试结果</div>
                <div className="test-step">
                    {TesterManager.GetNewControl(value.GroupName, value.ObjectName)}
                </div>
            </fieldset>
        </div>);
    }
}
abc_string.Register();
abc_ObjectA.Register();