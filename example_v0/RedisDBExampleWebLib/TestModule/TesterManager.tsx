import { PureComponent } from "react";
import Config from "../Config";
import Rest from "../Rest";
import React = require('react');

export class TestControl extends PureComponent<{ GroupName: string, ResourceObjectName: string }, {}> {
    constructor(objectKey, props?, states?) {
        super(props, states);
        this.ObjectKey = objectKey;
        let client = this.client = TesterManager.GetGroupRestClient(this.props.GroupName);
        if (!client.$[this.ObjectKey]) client.Uri.add(this.ObjectKey);
        this.$ = client.$;

        this.ReadRequestInfo = this.ParseRequestInfo(this.client
            , this.client.$[this.ObjectKey].read);

        this.DestroyRequestInfo = this.ParseRequestInfo(this.client
            , this.client.$[this.ObjectKey].del);

        this.UpdateRequestInfo = this.ParseRequestInfo(this.client
            , this.client.$[this.ObjectKey].update);

        this.CreateRequestInfo = this.ParseRequestInfo(this.client
            , this.client.$[this.ObjectKey].create);
    }
    readonly client: Rest;
    readonly $: JQueryRest.IRestRootResource;
    readonly ObjectKey = "";
    readonly ReadRequestInfo: string;
    readonly DestroyRequestInfo: string;
    readonly UpdateRequestInfo: string;
    readonly CreateRequestInfo: string;


    private ParseRequestInfo(clientCurrent: Rest, request: any) {
        let requestAct: any = request;
        let requestInstance = (requestAct as JQueryRest.IVerbFuncBasic).instance;
        let requestUrl = clientCurrent.$[this.ObjectKey].urlNoId;
        let requestMethod = requestInstance.method;
        let requestName = requestInstance.name;

        let requestInfo = `${requestMethod} ${requestUrl}[${requestName}]`;
        return requestInfo;
    }

}

export default class TesterManager {
    private static TestControlers: { [index: string]: any }
    private static ParseIndex(groupName: string, resourceObjectName: string): string {
        let newIndex = `${groupName}/${resourceObjectName}`;
        return newIndex;
    }
    static RegisterControl(groupName: string, resourceObjectName: string
        , controlType: any) {
        let newIndex = TesterManager.ParseIndex(groupName, resourceObjectName);
        TesterManager.TestControlers[newIndex] = controlType;
    }
    static GetNewControl(groupName: string, resourceObjectName: string):any {
        let index = TesterManager.ParseIndex(groupName, resourceObjectName);
        let classType = TesterManager.TestControlers[index];
        return classType ? React.createElement(classType, { GroupName: groupName, ResourceObjectName: resourceObjectName }) : null;
    }

    private static RestClients: { [index: string]: Rest}
    static GetGroupRestClient(groupName): Rest {
        let groupRestClient;
        groupRestClient = TesterManager.RestClients[groupName];
        if (groupRestClient) {
            return groupRestClient;
        } else {
            groupRestClient = new Rest(`${Config.TEST_ROOT_URI}${groupName}/`);
            TesterManager.RestClients[groupName] = groupRestClient;
            return groupRestClient;
        }
    }


    static Init() {
        TesterManager.TestControlers = {};
        TesterManager.RestClients = {};
    }

}

TesterManager.Init();