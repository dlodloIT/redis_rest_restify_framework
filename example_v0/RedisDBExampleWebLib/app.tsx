import RestTest from './RestTest';
import React = require('react');
import ReactDOM  = require('react-dom');

import Demo from './Demo';
import TestModule_RestList from './TestModule/RestList';

export default class APP extends React.Component<any, any> {
    render() {
        return <div>
            <TestModule_RestList />
            <h1>hello 4 {this.props.name} </h1><Demo name="demo01" /></div>;
    }
}

ReactDOM.render(<APP name="Tom" />, document.getElementById('react'));
