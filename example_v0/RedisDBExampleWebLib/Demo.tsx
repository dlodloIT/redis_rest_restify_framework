import React = require('react');
import { MouseEvent, ChangeEvent, Component, PureComponent } from 'react';

export default class Demo extends PureComponent<{ name: string }, { inputText: string, IsUser: boolean }> {
    constructor(props) {
        super(props);
        this.state = {
            inputText: "",
            IsUser: false
        }
    }
    UserName = "张三";
    inputControl: HTMLInputElement;
    handleClick(e: MouseEvent<HTMLInputElement>) {
        this.inputControl.focus();
    }
    handleCompontChangeClick(e: MouseEvent<HTMLInputElement>) {
        this.setState(prevState =>
            ({ IsUser: !prevState.IsUser })
        );
    }
    handleInputChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ inputText: e.target.value });
    }
    render() {
        let isUser = this.state.IsUser;
        let userName = this.UserName;
        let listValue = [31, 40, 87];
        //可以返回null,表示不输出,return null
        return <div>
            <fieldset><legend>属性绑定demo</legend>
                <div>this is a demo {this.props.name}</div>
            </fieldset>
            <fieldset><legend>不受控组件的事件绑定与状态自更新,将value绑定到state,则为受控组件</legend>
                <div>
                    <div><input type="text" ref={(input) => this.inputControl = input} defaultValue="请输入.."
                        onChange={(e) => this.handleInputChange(e)}
                    /></div>
                    <div>输入的内容:<span>{this.state.inputText}</span></div>
                    <div><input
                        type="button"
                        value="点我输入框获取焦点"
                        onClick={(e) => this.handleClick(e)}
                    /></div>
                </div>
            </fieldset>
            <fieldset><legend>动态组件输出和条件输出</legend>
                <div>
                    <div>
                        <Greeting IsUser={this.state.IsUser} />
                        {isUser && <span>{userName}</span>}
                    </div>
                    <div><input
                        type="button"
                        value="点我切换输出组件"
                        onClick={(e) => this.handleCompontChangeClick(e)}
                    /></div>
                </div>
            </fieldset>
            <fieldset><legend>列表map输出</legend>
                <div><Lists numbers={listValue} /></div>
            </fieldset>
            <div className="shopping-list">
                <h1>Shopping List for {this.props.name}</h1>
                <ul>
                    <li>Instagram</li>
                    <li>WhatsApp</li>
                    <li>Oculus</li>
                </ul>
            </div>
        </div>;
    }
}
class UserGreeting extends PureComponent<any, any>{
    render() {
        return <span>Welcome back!</span>;
    }
}
class GuestGreeting extends PureComponent<any, any>{
    render() {
        return <span>Please sign up.</span>
    }
}
class Greeting extends PureComponent<{ IsUser: boolean }, any>{
    render() {
        if (this.props.IsUser) {
            return <UserGreeting />;
        } else {
            return <GuestGreeting />;
        }
    }
}
class Lists extends PureComponent<{ numbers: number[] }, { numbers: number[] }>{
    constructor(props) {
        super(props);
        this.state = this.props;
    }
    render() {
        //使用以下语法:()=>**
        const listItems1 = this.state.numbers.map((value, index) => 
            <li key={index.toString()}>{value.toString()}</li>
        );
        //使用以下语法: ()=>{return **}
        const listItems2 = this.state.numbers.map((value, index) => {
            return <li key={(index + 10).toString()}>{value.toString()}</li>
        })
        return <div><ul>{listItems1}</ul><ul>{listItems2}</ul>
            <ul>
                {this.state.numbers.map((value, index) => 
                    <li key={(index + 20).toString()}>{value.toString()}</li>
                )}
            </ul>
        </div>
    }
}