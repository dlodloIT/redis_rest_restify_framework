export default class Config {
    public static readonly TEST_ROOT_URI = '/redis/';
    public static readonly TEST_MetaObjectName = "_meta";
    public static readonly TEST_OBJECT_TEST_STEP_SEARCH_LEFT = '#Object-';
    public static readonly TEST_OBJECT_TEST_STEP_SEARCH_RIGHT = ' div.test-step';
}