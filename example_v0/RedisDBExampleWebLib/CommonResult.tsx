import { PureComponent } from "react";
import * as React from "react";
import Rest from "Rest";

export default class CommonResult extends PureComponent<{ RequestInfo: string, Act: string }
    , { isDone: boolean, isSuccess: boolean, resultOrError: string }> {
    constructor(props) {
        super(props);
        this.state = { isDone: false, isSuccess: false, resultOrError: "" };
    }
    ParseRestError(err: JQueryRest.CallbackError) {
        let message = `[${err.status}-${err.statusText}]:${err.responseText}`;
        return message;
    }
    public Success(result: string | boolean) {
        let resultString = ""
        if (typeof result === "string") {
            resultString = result;
        } else {
            resultString = result + "";
        }
        this.setState({ isDone: true, isSuccess: true, resultOrError: resultString })
    }
    public Fail(err: JQueryRest.CallbackError | string) {
        let resultOrError = "";
        if (typeof err === "string") {
            resultOrError = err;
        } else {
            resultOrError = this.ParseRestError(err);
        }
        this.setState({ isDone: true, isSuccess: false, resultOrError: resultOrError })
    }
    public UpdateResult(isDone: boolean, isSuccess: boolean, resultOrError: string) {
        this.setState({ isDone: isDone, isSuccess: isSuccess, resultOrError: resultOrError })
    }
    render() {
        if (this.state.isDone) {
            if (this.state.isSuccess) {
                return <div>
                    <div><span className="requestOK">{this.props.Act}</span>&nbsp;{this.props.RequestInfo}</div>
                    <div>结果:{this.state.resultOrError}</div>
                </div>;
            } else {
                return <div>
                    <div><span className="requestFail">{this.props.Act}</span>&nbsp;{this.props.RequestInfo}</div>
                    <div>错误:{this.state.resultOrError}</div>
                </div>;
            }
        } else {
            return <div>{this.props.Act} 测试中...</div>
        }
    }
}
