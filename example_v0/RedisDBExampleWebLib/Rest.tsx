export default class Rest {
    ClientInstance: JQueryRest.IRestRootResource | JQueryRest.IRestResourceMore;
    $: JQueryRest.IRestRootResource;
    Uri: JQueryRest.IRestResourceMore;
    constructor(resourceUri: string) {
        this.ClientInstance = new $.RestClient(resourceUri);
        this.$ = this.ClientInstance as JQueryRest.IRestRootResource;
        this.Uri = this.ClientInstance as JQueryRest.IRestResourceMore;
    }
}