import rrr = require('@dlodlo/rrr');
import { Request, Response, Next } from 'restify';

interface IDataItem { id: number, a: number }
let data: Array<IDataItem> = [{ id: 771, a: 71111 }, { id: 772, a: 72 }, { id: 773, a: 72 }, { id: 774, a: 72 }, { id: 775, a: 72 }
    , { id: 776, a: 72 }, { id: 779, a: 72 }, { id: 7713, a: 72 }, { id: 7715, a: 72 }, { id: 7722, a: 72 }];

export default class IDArray extends rrr.RestObjectModule {

    Read(req: Request, res: Response, next: Next): void {
        let pageParams = this.GetPageParams(req, data.length);
        res.send(data.slice(pageParams.from, pageParams.to));
        return next();
        //throw new Error("Method not implemented.");
    }
    ReadByID(req: Request, res: Response, next: Next): void {
        var findOne = null;
        var id = req.params["id"];
        for (var i in data) {
            if (data[i].id == id) {
                findOne = data[i];
            }
        }
        res.send(findOne);
        return next();
        //throw new Error("Method not implemented.");
    }
    DelByID(req: Request, res: Response, next: Next): void {
        let findOne = null;
        let id = req.params["id"];
        for (let i:number = 0; i < data.length; i++) {
            if (data[i].id == id) {
                findOne = data.splice(i, 1);
                break;
            }
        }
        res.send(findOne);
        return next();
        //throw new Error("Method not implemented.");
    }
    Insert(req: Request, res: Response, next: Next): void {
        var id = req.params["id"];
        res.send({});
        return next();
        //throw new Error("Method not implemented.");
    }
    UpdateByID(req: Request, res: Response, next: Next): void {
        res.send({});
        return next();
        //throw new Error("Method not implemented.");
    }
}