import rrr = require('@dlodlo/rrr');

export interface IObjectMetaInfo {
    Name: string,
    Description: string
}

export let Objects: IObjectMetaInfo[] = [
    { Name: "IDArray", Description: "简单数组测试二" },
]



export default class _Meta extends rrr.RestObjectModule {
    Read(req: any, res: any, next: any): void {
        var objectData = Objects;
        res.send(objectData);
        next();
    }
    ReadByID(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }
    DelByID(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }
    Insert(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }
    UpdateByID(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }

}