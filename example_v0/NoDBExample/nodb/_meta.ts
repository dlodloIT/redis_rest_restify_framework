import rrr = require('@dlodlo/rrr');

export interface IGroupMetaInfo {
    Name: string,
    Description: string
}
export let Groups: IGroupMetaInfo[] = [
    { Name: "k1", Description: "K示例" },
    { Name: "t1", Description: "T示例" },
]


export default class _Meta extends rrr.RestObjectModule {
    Read(req: any, res: any, next: any): void {
        var groupData = Groups;
        res.send(groupData);
        res.end();
    }
    ReadByID(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }
    DelByID(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }
    Insert(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }
    UpdateByID(req: any, res: any, next: any): void {
        throw new Error("Method not implemented.");
    }

}