﻿/// <reference path="template-web.js" />
var RestTest = function () { };



RestTest.prototype.TestTree = function (objectTree) {
    for (var groupName in objectTree) {
        if (this['TestGroup_' + groupName]) {
            var groupClient = new $.RestClient(TEST_ROOT_URI + groupName + "/");
            var objectArray = objectTree[groupName];
            for (var i in objectArray) {
                var objectInfo = objectArray[i];
                this['TestGroup_' + groupName](groupClient, groupName, objectInfo);
            }
        } else {
            console.error('测试方法未实现:' + 'TestGroup_' + groupName);
        }
    }
};
RestTest.prototype.TestGroup_count = function (groupClient, groupName, objectInfo) {
    groupClient.add(objectInfo.ObjectName);
    var outputFinder = TEST_OBJECT_TEST_STEP_SEARCH_LEFT + objectInfo.GlobalIndex + TEST_OBJECT_TEST_STEP_SEARCH_RIGHT;
    var testOutput = $(outputFinder);
    testOutput.html('<div class="Read"></div><div class="ReadByID"></div><div class="Insert"></div><div class="DelByID"></div><div class="UpdateByID"></div>');
    var requestObject = groupClient[objectInfo.ObjectName];
    var requestRead = requestObject.read();
    var requestInstance = requestObject.read.instance;
    var requestUrl = requestObject.urlNoId;
    var requestMethod = requestInstance.method;
    var requestName = requestInstance.name;

    requestRead.done(function (data) {
        var output = $(outputFinder + " .Read");
        output.html(requestMethod + " " + requestUrl + " " + requestName + " OK");
    });
    requestRead.fail(function (error) {
        var output = $(outputFinder + " .Read");
        output.html('<div class="fail">' + requestMethod + " " + requestUrl + " " + requestName + " Fail</div>");
    });
};
RestTest.prototype.TestGroup_example = function (groupClient, groupName, objectInfo) {
    groupClient.add(objectInfo.ObjectName);
    var outputFinder = TEST_OBJECT_TEST_STEP_SEARCH_LEFT + objectInfo.GlobalIndex + TEST_OBJECT_TEST_STEP_SEARCH_RIGHT;
    var testOutput = $(outputFinder);
    testOutput.html('<div class="Read"></div><div class="ReadByID"></div><div class="Insert"></div><div class="DelByID"></div><div class="UpdateByID"></div>');
    var requestObject = groupClient[objectInfo.ObjectName];
    var requestRead = requestObject.read();
    var requestInstance = requestObject.read.instance;
    var requestUrl = requestObject.urlNoId;
    var requestMethod = requestInstance.method;
    var requestName = requestInstance.name;

    requestRead.done(function (data) {
        var output = $(outputFinder + " .Read");
        output.html(requestMethod + " " + requestUrl + " " + requestName + " OK");
    });
    requestRead.fail(function (error) {
        var output = $(outputFinder + " .Read");
        output.html('<div class="fail">' +requestMethod + " " + requestUrl + " " + requestName + " Fail</div>");
    });
};
RestTest.prototype.TestGroup_simple = function (groupClient, groupName, objectInfo) {
    groupClient.add(objectInfo.ObjectName);
    var outputFinder = TEST_OBJECT_TEST_STEP_SEARCH_LEFT + objectInfo.GlobalIndex + TEST_OBJECT_TEST_STEP_SEARCH_RIGHT;
    var testOutput = $(outputFinder);
    testOutput.html('<div class="Read"></div><div class="ReadByID"></div><div class="Insert"></div><div class="DelByID"></div><div class="UpdateByID"></div>');
    var requestObject = groupClient[objectInfo.ObjectName];
    var requestRead = requestObject.read();
    var requestInstance = requestObject.read.instance;
    var requestUrl = requestObject.urlNoId;
    var requestMethod = requestInstance.method;
    var requestName = requestInstance.name;
    requestRead.done(function (data) {
        var output = $(outputFinder + " .Read");
        output.html(requestMethod + " " + requestUrl + " " + requestName + " OK");
    });
    requestRead.fail(function (error) {
        var output = $(outputFinder + " .Read");
        output.html('<div class="fail">' + requestMethod + " " + requestUrl + " " + requestName + " Fail</div>");
    });
};

var client = new $.RestClient(TEST_ROOT_URI);
