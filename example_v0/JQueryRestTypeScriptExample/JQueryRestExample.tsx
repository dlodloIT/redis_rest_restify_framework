
class Test {
    public TestBasic() {
        var clientInstance = new $.RestClient('/rest/api/');
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;
        clientParent.add('foo');
        var client = clientInstance as JQueryRest.IRestRootResource;
        client.foo.read();
        // GET /rest/api/foo/
        client.foo.read(42);
        // GET /rest/api/foo/42/
        client.foo.read('forty-two');
        // GET /rest/api/foo/forty-two/
    }
    public TestRetrieving() {
        var clientInstance = new $.RestClient('/rest/api/');
        var client = clientInstance as JQueryRest.IRestRootResource;
        
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;
        clientParent.add('foo');

        var request = client.foo.read();
        var textStatus;
        var xhrObject;
        // GET /rest/api/foo/
        request.done(function (data, textStatus, xhrObject) {
            console.info('I have data: ' + data);
        });
        // OR simply:
        client.foo.read().done(function (data) {
            console.info('I have data: ' + data);
        });
    }
    public TestNestedResources() {
        var clientInstance = new $.RestClient('/rest/api/');
        var client = clientInstance as JQueryRest.IRestRootResource;
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;

        clientParent.add('foo');

        client.foo.add('baz');

        client.foo.read();
        // GET /rest/api/foo/
        client.foo.read(42);
        // GET /rest/api/foo/42/
        var baz = client.foo['baz'] as JQueryRest.IRestResourceSimple;
        baz.read();
        // GET /rest/api/foo/???/baz/???/
        // ERROR: jquery.rest: Invalid number of ID arguments, required 1 or 2, provided 0
        baz.read(42);
        // GET /rest/api/foo/42/baz/
        baz.read('forty-two', 21);
        // GET /rest/api/foo/forty-two/baz/21/

    }
    public TestBasicCRUDVerbs() {
        var clientInstance = new $.RestClient('/rest/api/');
        var client = clientInstance as JQueryRest.IRestRootResource;
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;

        clientParent.add('foo');

        // C
        client.foo.create({ a: 21, b: 42 });
        // POST /rest/api/foo/ (with data a=21 and b=42)
        // Note: data can also be stringified to: {"a":21,"b":42} in this case, see options below

        // R
        client.foo.read();
        // GET /rest/api/foo/
        client.foo.read(42);
        // GET /rest/api/foo/42/

        // U
        client.foo.update(42, { my: "updates" });
        // PUT /rest/api/42/   my=updates

        // D
        client.foo.destroy(42);
        client.foo.del(42);
        // DELETE /rest/api/foo/42/
        // Note: client.foo.delete() has been disabled due to IE compatibility

    }
    public TestAddingCustomVerbs() {
        var clientInstance = new $.RestClient('/rest/api/');
        var client = clientInstance as JQueryRest.IRestRootResource;
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;

        clientParent.add('foo');

        client.foo.addVerb('bang', 'PATCH');

        (client.foo['bang'] as JQueryRest.IVerbFuncOnlyQuery)({ my: "data" });
        //PATCH /foo/bang/   my=data
        (client.foo['bang'] as JQueryRest.IVerbFuncID1AndQuery)(42, { my: "data" });
        //PATCH /foo/42/bang/   my=data
    }
    public TestBasicAuthentication() {
        var clientInstance = new $.RestClient('/rest/api/', {
            username: 'admin',
            password: 'secr3t'
        });
        var client = clientInstance as JQueryRest.IRestRootResource;
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;

        clientParent.add('foo');

        client.foo.read();
        // GET /rest/api/foo/
        // With header "Authorization: Basic YWRtaW46c2VjcjN0"

    }
    public TestCaching() {
        var clientInstance = new $.RestClient('/rest/api/', {
            cache: 5, //This will cache requests for 5 seconds
            cachableMethods: ["GET"] //This defines what method types can be cached (this is already set by default)
        });
        var client = clientInstance as JQueryRest.IRestRootResource;
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;

        clientParent.add('foo');

        client.foo.read().done(function (data) {
            //'client.foo.read' is now cached for 5 seconds
        });

        // wait 3 seconds...

        client.foo.read().done(function (data) {
            //data returns instantly from cache
        });

        // wait another 3 seconds (total 6 seconds)...

        client.foo.read().done(function (data) {
            //'client.foo.read' cached result has expired
            //data is once again retrieved from the server
        });

        // Note: the cache can be cleared with:
        clientParent.cache.clear();
    }
    public TestOverrideOptions() {
        var clientInstance = new $.RestClient('/rest/api/');
        var client = clientInstance as JQueryRest.IRestRootResource;
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;

        clientParent.add('foo', {
            stripTrailingSlash: true,
            cache: 5
        });

        client.foo.add('bar', {
            cache: 10,
        });

        client.foo.read(21);
        // GET /rest/api/foo (strip trailing slash and uses a cache timeout of 5)

        var bar = client.foo['bar'] as JQueryRest.IRestResourceSimple;

        bar.read(7, 42);
        // GET /rest/api/foo/7/bar/42 (still strip trailing slash though now uses a cache timeout of 10)

    }
    public TestGetHttpRequestInfo() {
        var clientInstance = new $.RestClient('/rest/api/');
        var client = clientInstance as JQueryRest.IRestRootResource;
        var clientParent = clientInstance as JQueryRest.IRestResourceMore;
        clientParent.add('foo');

        var requestObject = client['foo'];
        var requestUrl = requestObject.urlNoId;

        var requestObjectAny = requestObject as any;
       
        var requestInstance = (requestObjectAny.read as JQueryRest.IVerbFuncBasic).instance;
        var requestMethod = requestInstance.method;
        var requestName = requestInstance.name;

    }
}
