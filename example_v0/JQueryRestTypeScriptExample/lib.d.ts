﻿declare namespace JQueryRest {

    interface IRestResourceMore {
        /**
         * Instaniates a nested resource on `client`. Internally this does another `new $.RestClient` though instead of setting it as root, it will add it as a nested (or child) resource as a property on the current `client`.
         * 
         * Newly created nested resources iterate through their `options.verbs` and addVerb on each.
         * 
         * Note: The url of each of these verbs is set to `""`.
         * 
         * See default `options.verbs` [here](https://github.com/jpillora/jquery.rest/blob/gh-pages/src/jquery.rest.coffee#L39).
         */
        add(name: string, options?: IClientObjectOption);
        /**
         * Instaniates a new Verb function property on the `client`.
         * 
         * Note: `name` is used as the `url` if `options.url` is not set.
         */
        addVerb(name: string, method: TVerbType, options?: IVerbOption);

        cache: ICacheManager;

        show();
    }
    interface ICacheManager {
        clear();
    }
    interface IRestRootResource {
        /**
         * Instantiates and returns the root resource. Below denoted as `client`.
         */
        new(rootUrl: string, options?: IClientOption): any;
        [name: string]: IRestResourceSimple;
    }
    /**
     * 第一级object
     */
    interface IRestResourceSimple extends IRestResourceMore {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'GET'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        read(id1?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         */
        read(id1?: StringOrNumber, id2?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         */
        read(id1?: StringOrNumber, id2?: StringOrNumber, id3?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'POST'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        create(queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         */
        create(data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'PUT'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        update(id1?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`params`])
         * 
         * verb = 'PUT'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        update(id1?: StringOrNumber, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'PUT'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        update(id1?: StringOrNumber, id2?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`params`])
         * 
         * verb = 'PUT'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        update(id1?: StringOrNumber, id2?: StringOrNumber, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        destroy(id1?: StringOrNumber, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        destroy(id1?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        destroy(id1?: StringOrNumber, id2?: StringOrNumber, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        destroy(id1?: StringOrNumber, id2?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;

        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        del(id1?: StringOrNumber, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        del(id1?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        del(id1?: StringOrNumber, id2?: StringOrNumber, queryParams?: any): IRestRequest;
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * 
         * verb = 'DELETE'
         * 
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        del(id1?: StringOrNumber, id2?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any): IRestRequest;
        /**
         * A plain object used as a `name` to `method` mapping.
         * The default `verbs` object
         * {
         *   'create': 'POST',
         *   'read'  : 'GET',
         *   'update': 'PUT',
         *   'delete': 'DELETE'
         * }
         * For example, to change the default behaviour of update from using PUT to instead use POST, set the `verbs` property to `{ update: 'POST' }`
         */
        urlNoId: string;
    }
    /**
     * 第一级object
     */
    type StringOrNumber = string | number;
    interface IVerbFuncInstance {
        method: string;
        name: string;
    }
    interface IVerbFuncBasic {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (id1?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any);
        instance: IVerbFuncInstance;
    }
    interface IVerbFuncOnlyQuery {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (queryParams?: any);
        instance: IVerbFuncInstance;
    }
    interface IVerbFuncID1AndQuery {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (id1?: StringOrNumber, queryParams?: any);
        instance: IVerbFuncInstance;
    }
    interface IVerbFuncIDs {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (id1?: StringOrNumber, ...idOthers: StringOrNumber[])
        instance: IVerbFuncInstance;
    }
    interface IVerbFuncID2Basic {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (id1?: StringOrNumber, id2?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any)
        instance: IVerbFuncInstance;
    }
    interface IVerbFuncID2AndQuery {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (id1?: StringOrNumber, id2?: StringOrNumber, queryParams?: any)
        instance: IVerbFuncInstance;
    }
    interface IVerbFuncID3Basic {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (id1?: StringOrNumber, id2?: StringOrNumber, id3?: StringOrNumber, data?: JQuery.AjaxSettings, queryParams?: any)
        instance: IVerbFuncInstance;
    }
    interface IVerbFuncID3AndQuery {
        /**
         *`client`.`verb`( [`id1`], [`id2`], [`id3`], [`data`], [`params`])
         * All verbs use this signature. Internally, they are all *essentially* calls to `$.ajax` with custom options depending on the parent `client` and `options`.
         *
         * `id`s must be a string or number.
         *
         * `data` is a [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object's data property. If `ajax.data` is set on the `client` this `data` will extend it.
         *
         * `params` query parameters to be appended to the url
         *
         * Note: A helpful error will be thrown if invalid arguments are used.
         */
        (id1?: StringOrNumber, id2?: StringOrNumber, id3?: StringOrNumber, queryParams?: any)
        instance: IVerbFuncInstance;
    }
    /**
     * The `options` object is a plain JavaScript option that may only contain the properties listed below.
     * 
     * See defaults [here](https://github.com/jpillora/jquery.rest/blob/gh-pages/src/jquery.rest.coffee#L32)
     * 
     * *Important*: Both resources and verbs inherit their parent's options !
     *
     */
    interface IClientCommonOption {
        /**
         * When both username and password are set, 
         * all ajax requests will add an 'Authorization' header. 
         * Encoded using `btoa` (polyfill required not non-webkit).
         * 
         * With header "Authorization: Basic YWRtaW46c2VjcjN0"
         */
        username?: string,
        /**
         * When both username and password are set, 
         * all ajax requests will add an 'Authorization' header. 
         * Encoded using `btoa` (polyfill required not non-webkit).
         * 
         * With header "Authorization: Basic YWRtaW46c2VjcjN0"
         */
        password?: string,
        /**
         * This will cache requests for cache seconds
         */
        cache?: number,
        /**
         * cachableMethods: ["GET"] 
         * 
         * //This defines what method types can be cached (this is already set by default)
         */
        cachableMethods?: TVerbType[];
        /**
         * A plain object used as a `name` to `method` mapping.
         * 
         * The default `verbs` object is set to:
         * ``` javascript
         * {
         *   'create': 'POST',
         *   'read'  : 'GET',
         *   'update': 'PUT',
         *   'delete': 'DELETE'
         * }
         * For example, to change the default behaviour of update from using PUT to instead use POST, 
         * set the `verbs` property to `{ update: 'POST' }`
         */
        verbs?: IOptionVerbs;
        /**
         * A string representing the URL for the given resource or verb.
         * 
         * Note: url is not inherited, if it is not set explicitly, the name is used as the URL.
         */
        url?: string;
        /**
         * When `true`, will pass all POST data through `JSON.stringify` (polyfill required for IE<=8).
         */
        stringifyData?: boolean;
        /**
         * When `true`, the trailing slash will be stripped off the URL.
         */
        stripTrailingSlash?: boolean;
        /**
         * The [jQuery Ajax](http://api.jquery.com/jQuery.ajax/) Options Object
         */
        ajax?: JQuery.AjaxSettings;
        /**
         * When `true`, requests (excluding HEAD and GET) become POST requests and the method chosen will be set as the header: `X-HTTP-Method-Override`. 
         * Useful for clients and/or servers that don't support certain HTTP methods.
         */
        methodOverride?: boolean;
        /**
         * The function used to perform the request (must return a jQuery Deferred). By default, it is:
         * 
         * ``` js
         * request: function(resource, options) {
         *   return $.ajax(options);
         * }
         * ```
         */
        request?: IFuncRequest;
        /**
         * When `true`, resource is perceived as singleton:
         */
        isSingle?: boolean;
        /**
         * When `false`, non-cachable requests (`PUT`, `POST` or `DELETE` - those not in `cachableMethods`) **won't** automatically clear the request's entry in the cache.
         */
        autoClearCache?: boolean;
    }
    interface IFuncRequest {
        (resource: any, options: any): any;
    }
    interface IOptionVerbs {
        [index: string]: TVerbType
    }
    /**
     * The `options` object is a plain JavaScript option that may only contain the properties listed below.
     * 
     * See defaults [here](https://github.com/jpillora/jquery.rest/blob/gh-pages/src/jquery.rest.coffee#L32)
     * 
     * *Important*: Both resources and verbs inherit their parent's options !
     *
     */
    interface IClientOption extends IClientCommonOption {
    }
    /**
     * The `options` object is a plain JavaScript option that may only contain the properties listed below.
     * 
     * See defaults [here](https://github.com/jpillora/jquery.rest/blob/gh-pages/src/jquery.rest.coffee#L32)
     * 
     * *Important*: Both resources and verbs inherit their parent's options !
     *
     */
    interface IClientObjectOption extends IClientCommonOption {
    }
    interface IVerbOption {
        url: string,
    }
    type TVerbType = "GET" | "POST" | "PUT" | "DELETE" | "PATCH";

    interface IRestRequest {
        done(callback: (data, textStatus?: any, xhrObject?: any) => void);
        fail(callback: (error, textStatus?: any, xhrObject?: any) => void);
    }
}
interface JQueryStatic {
    RestClient: JQueryRest.IRestRootResource
}
