var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Config", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Config = /** @class */ (function () {
        function Config() {
        }
        Config.TEST_ROOT_URI = '/redis/';
        Config.TEST_MetaObjectName = "_meta";
        Config.TEST_OBJECT_TEST_STEP_SEARCH_LEFT = '#Object-';
        Config.TEST_OBJECT_TEST_STEP_SEARCH_RIGHT = ' div.test-step';
        return Config;
    }());
    exports.default = Config;
});
define("RestTest", ["require", "exports", "Config"], function (require, exports, Config_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var RestTest = /** @class */ (function () {
        function RestTest() {
            this.Name = Config_1.default.TEST_MetaObjectName;
        }
        return RestTest;
    }());
    exports.default = RestTest;
});
define("Demo", ["require", "exports", "react", "react"], function (require, exports, React, react_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Demo = /** @class */ (function (_super) {
        __extends(Demo, _super);
        function Demo(props) {
            var _this = _super.call(this, props) || this;
            _this.UserName = "张三";
            _this.state = {
                inputText: "",
                IsUser: false
            };
            return _this;
        }
        Demo.prototype.handleClick = function (e) {
            this.inputControl.focus();
        };
        Demo.prototype.handleCompontChangeClick = function (e) {
            this.setState(function (prevState) {
                return ({ IsUser: !prevState.IsUser });
            });
        };
        Demo.prototype.handleInputChange = function (e) {
            this.setState({ inputText: e.target.value });
        };
        Demo.prototype.render = function () {
            var _this = this;
            var isUser = this.state.IsUser;
            var userName = this.UserName;
            var listValue = [31, 40, 87];
            //可以返回null,表示不输出,return null
            return React.createElement("div", null,
                React.createElement("fieldset", null,
                    React.createElement("legend", null, "\u5C5E\u6027\u7ED1\u5B9Ademo"),
                    React.createElement("div", null,
                        "this is a demo ",
                        this.props.name)),
                React.createElement("fieldset", null,
                    React.createElement("legend", null, "\u4E0D\u53D7\u63A7\u7EC4\u4EF6\u7684\u4E8B\u4EF6\u7ED1\u5B9A\u4E0E\u72B6\u6001\u81EA\u66F4\u65B0,\u5C06value\u7ED1\u5B9A\u5230state,\u5219\u4E3A\u53D7\u63A7\u7EC4\u4EF6"),
                    React.createElement("div", null,
                        React.createElement("div", null,
                            React.createElement("input", { type: "text", ref: function (input) { return _this.inputControl = input; }, defaultValue: "请输入..", onChange: function (e) { return _this.handleInputChange(e); } })),
                        React.createElement("div", null,
                            "\u8F93\u5165\u7684\u5185\u5BB9:",
                            React.createElement("span", null, this.state.inputText)),
                        React.createElement("div", null,
                            React.createElement("input", { type: "button", value: "点我输入框获取焦点", onClick: function (e) { return _this.handleClick(e); } })))),
                React.createElement("fieldset", null,
                    React.createElement("legend", null, "\u52A8\u6001\u7EC4\u4EF6\u8F93\u51FA\u548C\u6761\u4EF6\u8F93\u51FA"),
                    React.createElement("div", null,
                        React.createElement("div", null,
                            React.createElement(Greeting, { IsUser: this.state.IsUser }),
                            isUser && React.createElement("span", null, userName)),
                        React.createElement("div", null,
                            React.createElement("input", { type: "button", value: "点我切换输出组件", onClick: function (e) { return _this.handleCompontChangeClick(e); } })))),
                React.createElement("fieldset", null,
                    React.createElement("legend", null, "\u5217\u8868map\u8F93\u51FA"),
                    React.createElement("div", null,
                        React.createElement(Lists, { numbers: listValue }))),
                React.createElement("div", { className: "shopping-list" },
                    React.createElement("h1", null,
                        "Shopping List for ",
                        this.props.name),
                    React.createElement("ul", null,
                        React.createElement("li", null, "Instagram"),
                        React.createElement("li", null, "WhatsApp"),
                        React.createElement("li", null, "Oculus"))));
        };
        return Demo;
    }(react_1.PureComponent));
    exports.default = Demo;
    var UserGreeting = /** @class */ (function (_super) {
        __extends(UserGreeting, _super);
        function UserGreeting() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        UserGreeting.prototype.render = function () {
            return React.createElement("span", null, "Welcome back!");
        };
        return UserGreeting;
    }(react_1.PureComponent));
    var GuestGreeting = /** @class */ (function (_super) {
        __extends(GuestGreeting, _super);
        function GuestGreeting() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        GuestGreeting.prototype.render = function () {
            return React.createElement("span", null, "Please sign up.");
        };
        return GuestGreeting;
    }(react_1.PureComponent));
    var Greeting = /** @class */ (function (_super) {
        __extends(Greeting, _super);
        function Greeting() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Greeting.prototype.render = function () {
            if (this.props.IsUser) {
                return React.createElement(UserGreeting, null);
            }
            else {
                return React.createElement(GuestGreeting, null);
            }
        };
        return Greeting;
    }(react_1.PureComponent));
    var Lists = /** @class */ (function (_super) {
        __extends(Lists, _super);
        function Lists(props) {
            var _this = _super.call(this, props) || this;
            _this.state = _this.props;
            return _this;
        }
        Lists.prototype.render = function () {
            //使用以下语法:()=>**
            var listItems1 = this.state.numbers.map(function (value, index) {
                return React.createElement("li", { key: index.toString() }, value.toString());
            });
            //使用以下语法: ()=>{return **}
            var listItems2 = this.state.numbers.map(function (value, index) {
                return React.createElement("li", { key: (index + 10).toString() }, value.toString());
            });
            return React.createElement("div", null,
                React.createElement("ul", null, listItems1),
                React.createElement("ul", null, listItems2),
                React.createElement("ul", null, this.state.numbers.map(function (value, index) {
                    return React.createElement("li", { key: (index + 20).toString() }, value.toString());
                })));
        };
        return Lists;
    }(react_1.PureComponent));
});
define("Rest", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Rest = /** @class */ (function () {
        function Rest(resourceUri) {
            this.ClientInstance = new $.RestClient(resourceUri);
            this.$ = this.ClientInstance;
            this.Uri = this.ClientInstance;
        }
        return Rest;
    }());
    exports.default = Rest;
});
define("TestModule/TesterManager", ["require", "exports", "react", "Config", "Rest", "react"], function (require, exports, react_2, Config_2, Rest_1, React) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TestControl = /** @class */ (function (_super) {
        __extends(TestControl, _super);
        function TestControl(objectKey, props, states) {
            var _this = _super.call(this, props, states) || this;
            _this.ObjectKey = "";
            _this.ObjectKey = objectKey;
            var client = _this.client = TesterManager.GetGroupRestClient(_this.props.GroupName);
            if (!client.$[_this.ObjectKey])
                client.Uri.add(_this.ObjectKey);
            _this.$ = client.$;
            _this.ReadRequestInfo = _this.ParseRequestInfo(_this.client, _this.client.$[_this.ObjectKey].read);
            _this.DestroyRequestInfo = _this.ParseRequestInfo(_this.client, _this.client.$[_this.ObjectKey].del);
            _this.UpdateRequestInfo = _this.ParseRequestInfo(_this.client, _this.client.$[_this.ObjectKey].update);
            _this.CreateRequestInfo = _this.ParseRequestInfo(_this.client, _this.client.$[_this.ObjectKey].create);
            return _this;
        }
        TestControl.prototype.ParseRequestInfo = function (clientCurrent, request) {
            var requestAct = request;
            var requestInstance = requestAct.instance;
            var requestUrl = clientCurrent.$[this.ObjectKey].urlNoId;
            var requestMethod = requestInstance.method;
            var requestName = requestInstance.name;
            var requestInfo = requestMethod + " " + requestUrl + "[" + requestName + "]";
            return requestInfo;
        };
        return TestControl;
    }(react_2.PureComponent));
    exports.TestControl = TestControl;
    var TesterManager = /** @class */ (function () {
        function TesterManager() {
        }
        TesterManager.ParseIndex = function (groupName, resourceObjectName) {
            var newIndex = groupName + "/" + resourceObjectName;
            return newIndex;
        };
        TesterManager.RegisterControl = function (groupName, resourceObjectName, controlType) {
            var newIndex = TesterManager.ParseIndex(groupName, resourceObjectName);
            TesterManager.TestControlers[newIndex] = controlType;
        };
        TesterManager.GetNewControl = function (groupName, resourceObjectName) {
            var index = TesterManager.ParseIndex(groupName, resourceObjectName);
            var classType = TesterManager.TestControlers[index];
            return classType ? React.createElement(classType, { GroupName: groupName, ResourceObjectName: resourceObjectName }) : null;
        };
        TesterManager.GetGroupRestClient = function (groupName) {
            var groupRestClient;
            groupRestClient = TesterManager.RestClients[groupName];
            if (groupRestClient) {
                return groupRestClient;
            }
            else {
                groupRestClient = new Rest_1.default("" + Config_2.default.TEST_ROOT_URI + groupName + "/");
                TesterManager.RestClients[groupName] = groupRestClient;
                return groupRestClient;
            }
        };
        TesterManager.Init = function () {
            TesterManager.TestControlers = {};
            TesterManager.RestClients = {};
        };
        return TesterManager;
    }());
    exports.default = TesterManager;
    TesterManager.Init();
});
define("CommonResult", ["require", "exports", "react", "react"], function (require, exports, react_3, React) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var CommonResult = /** @class */ (function (_super) {
        __extends(CommonResult, _super);
        function CommonResult(props) {
            var _this = _super.call(this, props) || this;
            _this.state = { isDone: false, isSuccess: false, resultOrError: "" };
            return _this;
        }
        CommonResult.prototype.ParseRestError = function (err) {
            var message = "[" + err.status + "-" + err.statusText + "]:" + err.responseText;
            return message;
        };
        CommonResult.prototype.Success = function (result) {
            var resultString = "";
            if (typeof result === "string") {
                resultString = result;
            }
            else {
                resultString = result + "";
            }
            this.setState({ isDone: true, isSuccess: true, resultOrError: resultString });
        };
        CommonResult.prototype.Fail = function (err) {
            var resultOrError = "";
            if (typeof err === "string") {
                resultOrError = err;
            }
            else {
                resultOrError = this.ParseRestError(err);
            }
            this.setState({ isDone: true, isSuccess: false, resultOrError: resultOrError });
        };
        CommonResult.prototype.UpdateResult = function (isDone, isSuccess, resultOrError) {
            this.setState({ isDone: isDone, isSuccess: isSuccess, resultOrError: resultOrError });
        };
        CommonResult.prototype.render = function () {
            if (this.state.isDone) {
                if (this.state.isSuccess) {
                    return React.createElement("div", null,
                        React.createElement("div", null,
                            React.createElement("span", { className: "requestOK" }, this.props.Act),
                            "\u00A0",
                            this.props.RequestInfo),
                        React.createElement("div", null,
                            "\u7ED3\u679C:",
                            this.state.resultOrError));
                }
                else {
                    return React.createElement("div", null,
                        React.createElement("div", null,
                            React.createElement("span", { className: "requestFail" }, this.props.Act),
                            "\u00A0",
                            this.props.RequestInfo),
                        React.createElement("div", null,
                            "\u9519\u8BEF:",
                            this.state.resultOrError));
                }
            }
            else {
                return React.createElement("div", null,
                    this.props.Act,
                    " \u6D4B\u8BD5\u4E2D...");
            }
        };
        return CommonResult;
    }(react_3.PureComponent));
    exports.default = CommonResult;
});
define("TestModule/abc_string", ["require", "exports", "TestModule/TesterManager", "react", "CommonResult"], function (require, exports, TesterManager_1, React, CommonResult_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var abc_string = /** @class */ (function (_super) {
        __extends(abc_string, _super);
        function abc_string(props) {
            var _this = _super.call(this, "string", props) || this;
            _this.$.string.read().done(function (data) {
                _this.ReadViewControl.Success(data);
            }).fail(function (error) {
                _this.ReadViewControl.Fail(error);
            });
            _this.$.string.read(1).done(function (data) {
                _this.ReadByIDViewControl.Success(data);
            }).fail(function (error) {
                _this.ReadByIDViewControl.Fail(error);
            });
            _this.$.string.destroy(1).done(function (data) {
                if (data === true) {
                    _this.DelByIDViewControl.Success("true");
                }
                else {
                    _this.DelByIDViewControl.Fail("删除失败");
                }
            }).fail(function (error) {
                _this.DelByIDViewControl.Fail(error);
            });
            _this.$.string.update(1, { value: new Date().toLocaleTimeString() }).done(function (data) {
                _this.UpdateByIDViewControl.Success(data ? "true" : "false");
            }).fail(function (error) {
                _this.UpdateByIDViewControl.Fail(error);
            });
            return _this;
        }
        abc_string.Register = function () {
            TesterManager_1.default.RegisterControl("abc", "string", abc_string);
        };
        abc_string.prototype.render = function () {
            var _this = this;
            var a = React.createElement("div", null);
            return React.createElement("div", null,
                React.createElement(CommonResult_1.default, { RequestInfo: this.ReadRequestInfo, ref: function (el) { return _this.ReadViewControl = el; }, Act: "read" }),
                React.createElement(CommonResult_1.default, { RequestInfo: this.ReadRequestInfo, ref: function (el) { return _this.ReadByIDViewControl = el; }, Act: "read(1)" }),
                React.createElement(CommonResult_1.default, { RequestInfo: this.DestroyRequestInfo, ref: function (el) { return _this.DelByIDViewControl = el; }, Act: "del(1)" }),
                React.createElement(CommonResult_1.default, { RequestInfo: this.UpdateRequestInfo, ref: function (el) { return _this.UpdateByIDViewControl = el; }, Act: "update(1)" }));
        };
        return abc_string;
    }(TesterManager_1.TestControl));
    exports.default = abc_string;
});
define("TestModule/abc_ObjectA", ["require", "exports", "TestModule/TesterManager", "react", "CommonResult"], function (require, exports, TesterManager_2, React, CommonResult_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var abc_ObjectA = /** @class */ (function (_super) {
        __extends(abc_ObjectA, _super);
        function abc_ObjectA(props) {
            var _this = _super.call(this, "ObjectA", props) || this;
            var read = function () {
                _this.$.ObjectA.read().done(function (data) {
                    _this.crRead.Success(JSON.stringify(data));
                }).fail(function (error) {
                    _this.crRead.Fail(error);
                });
            };
            var destroyLast = function (lastKey) {
                _this.$.ObjectA.destroy(lastKey).done(function (result) {
                }).fail(function (error) {
                });
            };
            _this.$.ObjectA.create({ name: "测试项" }).done(function (createItem) {
                _this.crCreate.Success(JSON.stringify(createItem));
                destroyLast(createItem.id);
                read();
            }).fail(function (error) {
                _this.crCreate.Fail(error);
            });
            return _this;
            //this.$.ObjectA.create
        }
        abc_ObjectA.Register = function () {
            TesterManager_2.default.RegisterControl("abc", "ObjectA", abc_ObjectA);
        };
        abc_ObjectA.prototype.render = function () {
            var _this = this;
            return React.createElement("div", null,
                React.createElement(CommonResult_2.default, { RequestInfo: this.ReadRequestInfo, ref: function (el) { return _this.crRead = el; }, Act: "read" }),
                React.createElement(CommonResult_2.default, { RequestInfo: this.CreateRequestInfo, ref: function (el) { return _this.crCreate = el; }, Act: "create({ name:'测试项'})" }));
        };
        return abc_ObjectA;
    }(TesterManager_2.TestControl));
    exports.default = abc_ObjectA;
});
define("TestModule/RestList", ["require", "exports", "react", "react", "Config", "Rest", "TestModule/TesterManager", "TestModule/abc_string", "TestModule/abc_ObjectA"], function (require, exports, React, react_4, Config_3, Rest_2, TesterManager_3, abc_string_1, abc_ObjectA_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TestModule_RestList = /** @class */ (function (_super) {
        __extends(TestModule_RestList, _super);
        function TestModule_RestList(props) {
            var _this = _super.call(this, props) || this;
            _this.groupArray = [];
            _this.groupLoadedCount = 0;
            _this.objectGlobalIndex = 0;
            _this.restServices = [];
            _this.restServiceTree = {};
            _this.GetAllRestServices();
            return _this;
        }
        TestModule_RestList.prototype.GetAllRestServices = function () {
            var _this = this;
            var client = new Rest_2.default(Config_3.default.TEST_ROOT_URI);
            client.Uri.add(Config_3.default.TEST_MetaObjectName);
            client.$[Config_3.default.TEST_MetaObjectName].read().done(function (data) {
                _this.groupArray = data;
                for (var i in _this.groupArray) {
                    _this.LoadObjects(_this.groupArray[i]);
                }
            });
        };
        TestModule_RestList.prototype.LoadObjects = function (group) {
            var _this = this;
            var groupClient = new Rest_2.default(Config_3.default.TEST_ROOT_URI + group.Name + "/");
            groupClient.Uri.add(Config_3.default.TEST_MetaObjectName);
            var objectArray = [];
            var request = groupClient.$[Config_3.default.TEST_MetaObjectName].read();
            request.done(function (data) {
                objectArray = data;
                var objectInfoArray = [];
                for (var i in objectArray) {
                    var objectInfo = objectArray[i];
                    _this.objectGlobalIndex++;
                    var objectWithGroupInfo = {
                        RootUrl: Config_3.default.TEST_ROOT_URI,
                        GroupName: group.Name, ObjectName: objectInfo.Name, GlobalIndex: _this.objectGlobalIndex,
                        GroupDescription: group.Description,
                        ObjectDescription: objectInfo.Description
                    };
                    objectInfoArray.push(objectWithGroupInfo);
                    _this.restServices.push(objectWithGroupInfo);
                }
                _this.restServiceTree[group.Name] = objectInfoArray;
                _this.groupLoadedCount++;
                if (_this.groupLoadedCount == _this.groupArray.length) {
                    _this.RefreshUI();
                }
            });
            request.fail(function () {
                this.groupLoadedCount++;
                if (this.groupLoadedCount == this.groupArray.length) {
                    this.RefreshUI();
                }
            });
        };
        TestModule_RestList.prototype.RefreshUI = function () {
            this.refs.list.Change(this.restServices);
        };
        TestModule_RestList.prototype.render = function () {
            //let control = TesterManager.GetNewControl('abc', 'string');
            //
            return React.createElement("div", null,
                React.createElement("h1", null, "REST\u8D44\u6E90\u5BF9\u8C61\u6E05\u5355\u4E0E\u6D4B\u8BD5\u7ED3\u679C"),
                React.createElement("div", null,
                    React.createElement(RestResourceList, { ref: "list" })));
        };
        return TestModule_RestList;
    }(react_4.PureComponent));
    exports.default = TestModule_RestList;
    var RestResourceList = /** @class */ (function (_super) {
        __extends(RestResourceList, _super);
        function RestResourceList(props) {
            var _this = _super.call(this, props) || this;
            _this.state = { data: [] };
            return _this;
        }
        RestResourceList.prototype.Change = function (listData) {
            this.setState({ data: listData });
        };
        RestResourceList.prototype.render = function () {
            return this.state.data.map(function (value, index) { return React.createElement("div", { key: value.GlobalIndex, id: "Object-" + value.GlobalIndex },
                React.createElement("fieldset", null,
                    React.createElement("legend", null,
                        value.GlobalIndex,
                        " : ",
                        React.createElement("a", { href: value.RootUrl + value.GroupName + "/" + value.ObjectName },
                            "[GET]",
                            value.RootUrl,
                            value.GroupName,
                            "/",
                            value.ObjectName)),
                    React.createElement("div", null,
                        value.GroupDescription,
                        React.createElement("br", null),
                        value.ObjectDescription),
                    React.createElement("div", null, "\u81EA\u52A8\u5316\u6D4B\u8BD5\u7ED3\u679C"),
                    React.createElement("div", { className: "test-step" }, TesterManager_3.default.GetNewControl(value.GroupName, value.ObjectName)))); });
        };
        return RestResourceList;
    }(react_4.PureComponent));
    abc_string_1.default.Register();
    abc_ObjectA_1.default.Register();
});
define("app", ["require", "exports", "react", "react-dom", "Demo", "TestModule/RestList"], function (require, exports, React, ReactDOM, Demo_1, RestList_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var APP = /** @class */ (function (_super) {
        __extends(APP, _super);
        function APP() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        APP.prototype.render = function () {
            return React.createElement("div", null,
                React.createElement(RestList_1.default, null),
                React.createElement("h1", null,
                    "hello 4 ",
                    this.props.name,
                    " "),
                React.createElement(Demo_1.default, { name: "demo01" }));
        };
        return APP;
    }(React.Component));
    exports.default = APP;
    ReactDOM.render(React.createElement(APP, { name: "Tom" }), document.getElementById('react'));
});
//# sourceMappingURL=lib.js.map