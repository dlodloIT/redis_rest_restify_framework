//方便调试时自动监听指定文件的变化来实现页面自动刷新
//需要浏览器配合设置,IE中,需要设置缓存更新 为 每次访问网页时, chrome需要在开发工具Network中勾选Disable Cache
if (document.domain.indexOf('.') < 0) {
    requirejs(["io"], function (io) {
        var socketRootUrl = "http://"+document.location.hostname+":8000";
        var socket = io.connect(socketRootUrl);
        function Refresh() {
            SaveScrollTop();
            document.location.reload(true);
        }
        socket.on('debugFileChanged', function (data) {
            console.log(data);
            Refresh();
        });
        socket.on('debugModuleChanged', function (data) {
            console.log(data);
            Refresh();
        });
        var _h = 0;
        function SaveScrollTop() {
            _h = document.body.scrollTop || document.documentElement.scrollTop;
            SetCookie("s_#1", _h);
        }
        setTimeout(function (v) {
            document.body.scrollTop = document.documentElement.scrollTop
                = v;//页面加载时设置scrolltop高度
        }, 1000, GetCookie("s_#1"))
        function SetCookie(sName, sValue) {
            document.cookie = sName + "=" + escape(sValue) + "; ";
        }
        function GetCookie(sName) {
            var aCookie = document.cookie.split("; ");
            for (var i = 0; i < aCookie.length; i++) {
                var aCrumb = aCookie[i].split("=");
                if (sName == aCrumb[0])
                    return unescape(aCrumb[1]);
            }
            return 0;
        }
    })
}