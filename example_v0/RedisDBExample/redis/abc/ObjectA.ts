import RedisDBHandler from "../../RedisDBHandler";
import { Request, Response, Next } from "restify";

import * as Bluebird from 'bluebird';

import redis = require('redis');
import { ECDH } from "crypto";

Bluebird.promisifyAll(redis.RedisClient.prototype);
Bluebird.promisifyAll(redis.Multi.prototype);

interface Data {
    id: number;
    name: string;
}

export default class ObjectA extends RedisDBHandler {
    KEY: string = "ObjectA";
    INT_KEY: string = 'o$' + this.KEY + '$' + 'INT_KEY';
    HSET: string = 'o$' + this.KEY;
    Read(req: Request, res: Response, next: Next): void {
        this.client.smembers(this.HSET, (err: Error, keys: string[]) => {
            if (err) {
                res.sendRaw("false", {
                    'Content-Type': 'application/json'
                });
            } else {
                let list = [];
                let listCount = keys.length;
                let listNextIndex = 0;
                let sortKeys = keys.sort();
                sortKeys.forEach((key: string, index: number, array: string[]) => {
                    //this.client.hgetallAsync(value).then((obj: { [key: string]: string }) => {

                    //})
                    this.client.hgetall(key, (err: Error, obj: { [key: string]: string }) => {
                        listNextIndex++;
                        if (!err)
                        {
                            list.push(obj);
                        }
                        if (listNextIndex >= listCount) {
                            res.send(list);
                        }
                    })
                })                
            }
        });
    }
    ReadByID(req: Request, res: Response, next: Next): void {
        res.send(false);
    }
    DelByID(req: Request, res: Response, next: Next): void {
        let id = req.params.id;
        if (id == null || id.length == 0) {
            next(new Error("req.params.id is empty!"))
            return;
        }
        let itemKey = this.HSET + ':' + id;
        let multi = this.client.MULTI();
        multi.srem(this.HSET, itemKey);
        multi.hdel(itemKey);

        multi.execAsync().then((replay: any[]) => {
            res.send(true);
        }).catch((err) => {
            res.sendRaw("false", {
                'Content-Type': 'application/json'
            });
        });
    }
    Insert(req: Request, res: Response, next: Next): void {
        let requestName = req.body.name;

        if (requestName == null || requestName.length == 0) {
            next(new Error("req.body.name is empty!"))
            return;
        }

        this.client.watch(this.INT_KEY);
        let multi = this.client.MULTI();

        this.client.incr(this.INT_KEY, (err, id: number) => {
            this.client.unwatch();
            let data: Data = { id: id, name: requestName };
            let itemKey = this.HSET + ':' + data.id;
            multi.sadd(this.HSET, itemKey);
            multi.hset(itemKey, 'id', data.id + '');
            multi.hset(itemKey, 'name', data.name);
            multi.exec((err, replay: any[]) => {
                if (err) {
                } else {
                }
            })
            multi.execAsync().then((replay: any[]) => {
                res.send({ id: data.id });
            }).catch((err) => {
                res.sendRaw("false", {
                    'Content-Type': 'application/json'
                });
            });
        })
    }
    UpdateByID(req: Request, res: Response, next: Next): void {
        res.sendRaw("false", {
            'Content-Type': 'application/json'
        });
    }

}