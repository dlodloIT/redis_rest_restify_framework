import RedisDBHandler from "../../RedisDBHandler";
import { Request, Response, Next } from 'restify';

const KEY = "default-string";

export default class StringExample extends RedisDBHandler {
    Read(req: Request, res: Response, next: Next): void {
        //next(new Error("test 1"));
        this.client.get(KEY, (err, reply) => {
            if (err) {
                next(err);
            } else {
                res.send(reply);
            }
        });
    }
    ReadByID(req: Request, res: Response, next: Next): void {
        this.client.get(KEY, (err, reply) => {
            if (err) {
                next(err);
            } else {
                res.send(reply);
            }
        });
    }
    DelByID(req: Request, res: Response, next: Next): void {
        this.client.del(KEY, (err, replay) => {
            if (err) {
                res.send(false);
            } else {
                res.send(true);
            }
        })
    }
    Insert(req: Request, res: Response, next: Next): void {
        res.send(false);
        next();
    }
    UpdateByID(req: Request, res: Response, next: Next): void {
        this.client.set(KEY, req.body['value'], (err, replay) => {
            if (err) {
                res.send(false);
            } else {
                res.send(true);
            }
        });
    }
}