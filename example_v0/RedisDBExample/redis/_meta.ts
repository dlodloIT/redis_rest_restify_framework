import GroupsMeta, { IGroupMetaInfo } from '../GroupsMeta';

export default class _Meta extends GroupsMeta {
    OnInitData() {
        this.groupsData = require('./_metaJSON');
    }
}