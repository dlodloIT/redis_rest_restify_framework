import rrr = require('@dlodlo/rrr');
import { Request, Response, Next } from 'restify';

export interface IObjectMetaInfo {
    Name: string,
    Description: string
}

export default abstract class ObjectsMeta extends rrr.RestObjectModule {
    protected objectsData: IObjectMetaInfo[];
    public constructor() {
        super();
        this.OnInitData();
    }
    /**
     * 对this.groupData进行赋值
     */
    abstract OnInitData();

    Read(req: Request, res: Response, next: Next): void {
        res.send(this.objectsData);
        res.end();
    }
    ReadByID(req: Request, res: Response, next: Next): void {
        let findOne = null;
        let id = req.params["id"];
        for (let i in this.objectsData) {
            if (this.objectsData[i].Name == id) {
                findOne = this.objectsData[i];
                break;
            }
        }
        res.send(findOne);
        return next();
    }
    DelByID(req: Request, res: Response, next: Next): void {
        throw new Error("不支持删除数据对象.");
    }
    Insert(req: Request, res: Response, next: Next): void {
        throw new Error("不支持创建新的数据对象");
    }
    UpdateByID(req: Request, res: Response, next: Next): void {
        throw new Error("不支持更新数据对象");
    }

}