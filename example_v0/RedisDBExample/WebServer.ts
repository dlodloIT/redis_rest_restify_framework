import { ServerA } from "@dlodlo/rrr";

export default class WebServer extends ServerA {
    public Listen() {
        super.Listen();
        if (this.config.AppConfig.DEBUG_MODE) {
            this.ListenDebugTSX();
        }
    }
    ListenDebugTSX() {
        this.Server.get('/RedisDBExampleWebLib/*.ts*', this.serveStatic({
            directory: this.config.AppConfig.APP_ROOT + '/../'
        }));
    }
}
