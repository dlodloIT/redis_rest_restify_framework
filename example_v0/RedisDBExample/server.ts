import { Config} from "@dlodlo/rrr";
import WebServer from "./WebServer";

Config.AppConfig.APP_ROOT = __dirname;
Config.AppConfig.DEBUG_WATCH_FILE = __dirname + Config.AppConfig.DEBUG_WATCH_FILE;
Config.ServerConfig.HOST = '::';
Config.ServerConfig.SERVICE_ROOT_01 = '/redis';
Config.ServerConfig.VIRTUAL_ROOT = '/redis';

let server = new WebServer(Config);

server.Ready();
server.Listen();