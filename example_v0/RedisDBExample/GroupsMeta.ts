import rrr = require('@dlodlo/rrr');
import { Request, Response, Next } from 'restify';

export interface IGroupMetaInfo {
    Name: string,
    Description: string
}

export default abstract class GroupsMeta extends rrr.RestObjectModule {
    protected groupsData: IGroupMetaInfo[];
    public constructor() {
        super();
        this.OnInitData();
    }
    /**
     * 对this.groupData进行赋值
     */
    abstract OnInitData();

    Read(req: Request, res: Response, next: Next): void {
        res.send(this.groupsData);
        res.end();
    }
    ReadByID(req: Request, res: Response, next: Next): void {
        let findOne = null;
        let id = req.params["id"];
        for (let i in this.groupsData) {
            if (this.groupsData[i].Name == id) {
                findOne = this.groupsData[i];
                break;
            }
        }
        res.send(findOne);
        return next();
    }
    DelByID(req: Request, res: Response, next: Next): void {
        throw new Error("不支持删除组对象.");
    }
    Insert(req: Request, res: Response, next: Next): void {
        throw new Error("不支持创建新的组对象");
    }
    UpdateByID(req: Request, res: Response, next: Next): void {
        throw new Error("不支持更新组对象");
    }

}