import { RestObjectModule,  Config } from '@dlodlo/rrr';
import { Request, Response, Next } from 'restify';
import redis = require('redis');

export default abstract class RedisDBHandler extends RestObjectModule {
    static redisClient: redis.RedisClient = null;
    static redisDBConntected: boolean = false;
    protected client: redis.RedisClient = null;
    static REDIS_PORT: number = 6379;
    static REDIS_HOST: string = '127.0.0.1';
    static REDIS_DB_INDEX: number = 1;

    public constructor() {
        super();
        this.InitDB();
    }
    protected InitDB() {
        if (RedisDBHandler.redisDBConntected == false) {
            let port = RedisDBHandler.REDIS_PORT;
            let host = RedisDBHandler.REDIS_HOST;
            let dbIndex = RedisDBHandler.REDIS_DB_INDEX;
            this.client = redis.createClient(port, host);
            RedisDBHandler.redisClient = this.client;
            this.client.select(dbIndex, (err, text) => {
                if (err) {
                    console.log(`redis select db (${dbIndex}) error: ` + err.message);
                }
            });
            this.client.on("error", (error) => {
                console.log(`redis connect (${host}:${port}) error: ` + error);
                RedisDBHandler.redisDBConntected = false;
                RedisDBHandler.redisClient = null;
                this.client = null;
            });
        } else {
            this.client = RedisDBHandler.redisClient;
        }

        RedisDBHandler.redisDBConntected = true;
    }
    
    /**
     * 读取记录清单集合
     * @param req
     * @param res
     * @param next
     */
    abstract Read(req: Request, res: Response, next: Next): void;
    /**
     * 按标识读取指定一条记录
     * @param req
     * @param res
     * @param next
     */
    abstract ReadByID(req: Request, res: Response, next: Next): void;
    /**
     * 按标识删除指定一条记录
     * @param req
     * @param res
     * @param next
     */
    abstract DelByID(req: Request, res: Response, next: Next): void;
    /**
     * 追加一条新记录
     * @param req
     * @param res
     * @param next
     */
    abstract Insert(req: Request, res: Response, next: Next): void;
    /**
     * 更新记录
     * @param req
     * @param res
     * @param next
     */
    abstract UpdateByID(req: Request, res: Response, next: Next): void;
}
