import rrr = require('@dlodlo/rrr');
import { Request, Response, Next } from 'restify';

/**
 * { id: number, a: number }
 */
export interface IDataItem { id: number, a: number }

export default abstract class IDArrayHandler extends rrr.RestObjectModule {
    protected idArrayData: Array<IDataItem> = [];
    public constructor() {
        super();
        this.OnInitData();
    }
    /**
     * 对this.idArrayData进行赋值
     */
    abstract OnInitData();
    /**
     * 读取记录清单集合,支持分页读取
     * @param req
     * @param res
     * @param next
     */
    Read(req: Request, res: Response, next: Next): void {
        let pageParams = this.GetPageParams(req, this.idArrayData.length);
        res.send(this.idArrayData.slice(pageParams.from, pageParams.to));
        return next();
        //throw new Error("Method not implemented.");
    }
    /**
     * 按标识读取指定一条记录
     * @param req
     * @param res
     * @param next
     */
    ReadByID(req: Request, res: Response, next: Next): void {
        var findOne = null;
        var id = req.params["id"];
        for (var i in this.idArrayData) {
            if (this.idArrayData[i].id == id) {
                findOne = this.idArrayData[i];
                break;
            }
        }
        res.send(findOne);
        return next();
        //throw new Error("Method not implemented.");
    }
    /**
     * 按标识删除指定一条记录
     * @param req
     * @param res
     * @param next
     */
    DelByID(req: Request, res: Response, next: Next): void {
        let findOne = null;
        let id = req.params["id"];
        for (let i: number = 0; i < this.idArrayData.length; i++) {
            if (this.idArrayData[i].id == id) {
                findOne = this.idArrayData.splice(i, 1);
                break;
            }
        }
        res.send(findOne);
        return next();
        //throw new Error("Method not implemented.");
    }
    /**
     * 追加一条新记录
     * @param req
     * @param res
     * @param next
     */
    Insert(req: Request, res: Response, next: Next): void {
        var id = req.params["id"];
        res.send({});
        return next();
        //throw new Error("Method not implemented.");
    }
    /**
     * 更新记录
     * @param req
     * @param res
     * @param next
     */
    UpdateByID(req: Request, res: Response, next: Next): void {
        res.send({});
        return next();
        //throw new Error("Method not implemented.");
    }
}
