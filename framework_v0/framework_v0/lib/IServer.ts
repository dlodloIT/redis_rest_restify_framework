﻿export default interface IServer {
    /**
     * 准备服务器资源对象及基本配置
     */
    Ready();
    /**
     * 初始化路由,开始监听,
     */
    Listen();
}