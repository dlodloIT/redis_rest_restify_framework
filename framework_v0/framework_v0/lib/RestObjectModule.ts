﻿import events = require("events");
import restify = require('restify');

export interface IPageInfo {
    /** 当前始于某一条,从0开始编号 */
    from: number,
    /** 当前结束于某一条,从0开始编号 */
    to: number
}

export default abstract class RestObjectModule extends events.EventEmitter{
    /**
     * 提取Request.query中的["from"],["to"]或["p"]页码与["s"](可选,默认每页20)进行分页计算
     * @param req
     * @param maxCount 总记录数,默认总记录数20条,在使用from,to模式下可以不提供总记录数
     */
    protected GetPageParams(req: restify.Request, maxCount: number = 20): IPageInfo {
        let myRequest: restify.Request = req;
        let from:number = 0;
        let to:number = maxCount - 1;
        let requestPageIndex:number;
        let requestPageSize: number;
        let pageIndex: number;
        let pageSize: number;
        let mayMaxPage: number;
        let maxPage: number;
        let hasMorePage: boolean;
        if (myRequest.query["from"] && myRequest.query["to"]) {
            from = myRequest.query["from"];
            to = myRequest.query["to"];
        } else if (myRequest.query["p"]) {
            requestPageIndex = myRequest.query["p"] || 1;
            requestPageSize = myRequest.query["s"] || 20;
            pageIndex = requestPageIndex;
            pageSize = requestPageSize;
            //fix pageIndex;
            mayMaxPage = Math.floor(maxCount / pageSize);
            hasMorePage = maxCount > mayMaxPage * pageSize;
            maxPage = mayMaxPage + (hasMorePage ? 1 : 0);
            if (pageIndex < 1) pageIndex = 1;
            if (pageIndex > maxPage) pageIndex = maxPage;
            from = (pageIndex - 1) * pageSize;
            to = pageIndex * pageSize;
        }
        let pageInfo: IPageInfo = { from: from, to: to }
        return pageInfo;
    }
    /**
     * 读取记录清单集合
     * @param req
     * @param res
     * @param next
     */
    abstract Read(req: restify.Request, res: restify.Response, next: restify.Next): void;
    /**
     * 按标识读取指定一条记录
     * @param req
     * @param res
     * @param next
     */
    abstract ReadByID(req: restify.Request, res: restify.Response, next: restify.Next): void;
    /**
     * 按标识删除指定一条记录
     * @param req
     * @param res
     * @param next
     */
    abstract DelByID(req: restify.Request, res: restify.Response, next: restify.Next): void;
    /**
     * 追加一条新记录
     * @param req
     * @param res
     * @param next
     */
    abstract Insert(req: restify.Request, res: restify.Response, next: restify.Next): void;
    /**
     * 更新记录
     * @param req
     * @param res
     * @param next
     */
    abstract UpdateByID(req: restify.Request, res: restify.Response, next: restify.Next): void;
};
