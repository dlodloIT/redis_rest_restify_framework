import fs = require("fs");
import path = require("path");
import util = require("util");
import events = require("events");

interface IModuleWatch {
    Close();
}
interface IModuleWatchCache {
    [index: string]: IModuleWatch
}
export default class ModuleWatch extends events.EventEmitter implements IModuleWatch {
    public static Caches: IModuleWatchCache = {}
    public static readonly EVENTS_RELOAD = 'reload';
    public static AnyModuleReloadCallback(modulePath) { }

    private DataFile: string;
    private tryRequireTimer: any = null;
    private fsWatch: fs.FSWatcher = null;

    public constructor(file: string){
        super();
        this.DataFile = path.normalize(file);
        try {
            require(this.DataFile);
        } catch (ex) {
            console.error("Require file exception!\t" + ex.message);
        }
        if (ModuleWatch.Caches[this.DataFile] !== undefined) {
            let watch: IModuleWatch = ModuleWatch.Caches[this.DataFile];
            watch.Close();
        }
        ModuleWatch.Caches[this.DataFile] = this;
        var defaultHandler = () => {
            console.log("Reload OK:\t" + this.DataFile);
            if (ModuleWatch.AnyModuleReloadCallback) {
                ModuleWatch.AnyModuleReloadCallback(this.DataFile);
            }
        };
        this.on(ModuleWatch.EVENTS_RELOAD, defaultHandler);
        console.log("Watching module:\t" + this.DataFile);
        this.fsWatch = fs.watch(this.DataFile, (eventType: string, fileName: string) => {
            this.fileChanged(eventType, fileName);
        });
   }
    private fileChanged(eventType:string, fileName:string) {
        this.tryRequire();
    }
    private tryRequire() {//解除文件变化时快速重复多次事件触发
        if (this.tryRequireTimer !== null) clearTimeout(this.tryRequireTimer);
        this.tryRequireTimer = setTimeout(() => { this.executeRequireAgain(); }, 200);
    }
    private executeRequireAgain() {
        if (this.tryRequireTimer !== null) clearTimeout(this.tryRequireTimer);
        console.log("Reload module:\t" + this.DataFile);
        this.requireAgain();
    }
    private requireAgain() {
        var requireCachePath = path.resolve(this.DataFile);
        delete require.cache[requireCachePath];
        try {
            require(requireCachePath);
        } catch (ex) {
            console.error("Exception!\t" + ex.message);
        }
        this.emit(ModuleWatch.EVENTS_RELOAD, this);
    };
    public Close() {
        console.log("Close watch module:\t" + this.DataFile);
        this.fsWatch.close();
        delete (ModuleWatch.Caches[this.DataFile]);
    }
}