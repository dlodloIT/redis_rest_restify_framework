const FILE_ROUTES_LOCK: string = "/routes.lock.js";

import fs = require("fs");
import path = require("path");
import restify = require("restify");
import events = require("events");
import Routes = require("./routes");
import { IConfig } from "./config";
import $routes from "./routes";
import ModuleWatch from "./ModuleWatch";
import ModuleLoader from "./ModuleLoader";

import IServer from "./IServer";
import marked = require('marked');
import MarkdownLoader from "./MarkdownLoader";
import MarkdownCache from "./MarkdownCache";
import { Renderer } from "marked";
import { error } from "util";
import { plugins, Route } from "restify";
import SocketIO = require("socket.io");
import { clearTimeout, setTimeout } from "timers";

const CONTENT_DB_REST_HOME = "DB REST home";
const CONTENT_EXCEPTION_DB_REST = "DB REST exception!";

const ACTION_INVALID_ERROR_TITLE_DB_REST = "DB REST Action Invalid Error";
const ACTION_INVALID_ERROR_CONTENT_DB_REST = "nodejs action of this module of this request is invalid now, action:"

const MODULE_INVALID_ERROR_TITLE_DB_REST = "DB REST Module Invalid Error";
const MODULE_INVALID_ERROR_CONTENT_DB_REST = "nodejs module of this request is invalid now, exception:"

interface ServeStatic {
    appendRequestPath?: boolean;
    directory?: string;
    maxAge?: number;
    match?: any;
    charSet?: string;
    file?: string;
    etag?: string;
    default?: any;
    gzip?: boolean;
}


export default class ServerB extends events.EventEmitter implements IServer {
    public readonly APP_ROOT: string;
    public readonly DEBUG_WATCH_FILE: string;
    public readonly VIRTUAL_ROOT: string;
    public readonly SERVICE_ROOT_01: string;
    public readonly config: IConfig;

    protected Server: restify.Server;
    protected SocketServer: SocketIO.Server = null;
    protected SocketRestifyServer: restify.Server = null;
    protected Socket: SocketIO.Socket = null;

    public MarkdownFileHeader: string = "";
    public MarkdownFileEnd: string = "";

    constructor(config:IConfig) {
        super();
        if (config.AppConfig.APP_ROOT == null) {
            throw new error('必须通过__dirname对AppConfig.APP_ROOT赋值,否则无法正常启动服务.');
        }
        this.APP_ROOT = config.AppConfig.APP_ROOT;
        this.VIRTUAL_ROOT = config.ServerConfig.VIRTUAL_ROOT;
        this.SERVICE_ROOT_01 = config.ServerConfig.SERVICE_ROOT_01;
        this.DEBUG_WATCH_FILE = path.resolve(config.AppConfig.DEBUG_WATCH_FILE);
        this.config = config;
    }
    public Ready() {

        this.Server = restify.createServer({
            name: this.config.AppConfig.APP_NAME,
            versions: [this.config.ServerConfig.RESTIFY_SERVER_DEFAULT_ROUTE_VERSION]
        });
        this.Server.use(restify.plugins.acceptParser(this.Server.acceptable));
        this.Server.use(restify.pre.userAgentConnection());// work around for curl
        this.Server.use(restify.plugins.bodyParser());
        this.Server.use(restify.plugins.queryParser());
        this.Server.use(restify.plugins.jsonp());

        this.SocketRestifyServer = restify.createServer({ socketio: true, name: this.config.AppConfig.APP_NAME + ":socket.io", version: "1.0"});
        this.SocketServer = SocketIO(this.SocketRestifyServer);

        ModuleWatch.AnyModuleReloadCallback = (modulePath) => {
            this.ModuleChanged(modulePath);
        };
    }
    private ModuleChanged(modulePath) {
        if (this.Socket) {
            console.log("debug module changed: " + modulePath);
            this.Socket.emit("debugModuleChanged", modulePath + " changed.");
        }
    }
    private KeepDebugWatchTimer = null;
    private KeepDebugWatch() {//解除文件变化时快速重复多次事件触发
        if (this.KeepDebugWatchTimer) clearTimeout(this.KeepDebugWatchTimer);
        this.KeepDebugWatchTimer = setTimeout(() => { this.KeepDebugWatchGo(); }, 200)
    }
    private KeepDebugWatchGo() {
        if (this.Socket) {
            console.log("debug file changed: " + this.DEBUG_WATCH_FILE);
            this.Socket.emit("debugFileChanged", this.DEBUG_WATCH_FILE + " changed.");
        }
    }
    public Listen() {
        ModuleLoader.WatchModuleOfDirectory(this.APP_ROOT + this.SERVICE_ROOT_01);

        this.LoadRootRoutes();
        this.LoadConfigRoutes();

        var routeFileWatch = new ModuleWatch(this.APP_ROOT + FILE_ROUTES_LOCK);
        routeFileWatch.on(ModuleWatch.EVENTS_RELOAD, () => { this.ReloadALlRoutes(); });
        this.Server.listen(this.config.ServerConfig.PORT, this.config.ServerConfig.HOST, () => {
            console.log('server start on 0.0.0.0:' + this.config.ServerConfig.PORT + ", url:" + this.Server.url + this.VIRTUAL_ROOT);
        });
        this.ListenSocket();
        console.log("watch debug file: " + this.DEBUG_WATCH_FILE);
        fs.watch(this.DEBUG_WATCH_FILE, (eventType: string, fileName: string) => {
            this.KeepDebugWatch();
        });
    }
    private ListenSocket() {
        if (this.SocketServer) {
            this.SocketRestifyServer.listen(8000, () => console.log('socket.io server listening at %s', this.SocketRestifyServer.url));
            this.SocketServer.on("connection", (socket) => {
                this.Socket = socket;
            });
        }
    }

    private AllRouters: any[] = [];
    private ReloadALlRoutes() {
        this.AllRouters.forEach((route, index) => {
            this.Server.rm(route);
        })
        this.LoadConfigRoutes();
    }

    private LoadRootRoutes() {

        this.Server.get('/', (req, res, next) => {
            res.redirect(301, '/index.html', next);
            return next();
        });

        this.Server.get('/*.html', restify.plugins.serveStatic({
            directory: this.config.AppConfig.APP_ROOT + this.config.ServerConfig.PUBLIC_STATIC_DIRECTORY
        }));

        this.Server.get(this.VIRTUAL_ROOT, restify.plugins.serveStatic({
            directory: this.config.AppConfig.APP_ROOT + this.config.ServerConfig.PUBLIC_STATIC_DIRECTORY
        }));

        this.Server.get(this.VIRTUAL_ROOT + '/*', restify.plugins.serveStatic({
            directory: this.config.AppConfig.APP_ROOT + this.config.ServerConfig.PUBLIC_STATIC_DIRECTORY
        }));

        this.Server.get('/docs/*.html', restify.plugins.serveStatic({
            directory: this.config.AppConfig.APP_ROOT + this.config.ServerConfig.PUBLIC_STATIC_DIRECTORY
        }));

        this.Server.get('/css/*.css', restify.plugins.serveStatic({
            directory: this.config.AppConfig.APP_ROOT + this.config.ServerConfig.PUBLIC_STATIC_DIRECTORY
        }));

        this.Server.get('/js/*.js', restify.plugins.serveStatic({
            directory: this.config.AppConfig.APP_ROOT + this.config.ServerConfig.PUBLIC_STATIC_DIRECTORY
        }));
        let markdownLoader = new MarkdownLoader();
        let markdownDirectory = this.config.AppConfig.APP_ROOT + this.config.ServerConfig.PUBLIC_STATIC_DIRECTORY + '/md/';
        markdownLoader.Load(markdownDirectory);
        let mdResponse = (req, res, next) => {
            let mdFileName = req.params['file'];
            let markdownContent = MarkdownCache.Get(markdownDirectory + mdFileName + ".md");
            let targetContent = "";

            {//处理Include
                let arr = markdownContent.split('{include(');
                targetContent = arr[0];
                for (let i = 1; i < arr.length; i++) {
                    let endPosition = arr[i].indexOf(')}');
                    let includeContent = "";
                    if (endPosition > 0) {
                        let fileName = arr[i].substr(0, endPosition);
                        includeContent = MarkdownCache.Get(markdownDirectory + fileName + ".md");
                    }
                    if (includeContent == "") {
                        targetContent += "{include(" + arr[i];
                    } else {
                        targetContent += includeContent + arr[i].substr(endPosition + 2);
                    }
                }
            }
            //{//处理wiki自动关联[[text:wiki]],但在所有Md都在同级时,直接用[中文](中文说明),这样的直接支持的方式表达,更清晰,也更方便在编辑时测试跳转
            //    let START = '[[';
            //    let END = ']]';
            //    let arr = targetContent.split(START);
            //    targetContent = arr[0];
            //    for (let j = 1; j < arr.length; j++) {
            //        let endPosition = arr[j].indexOf(END);
            //        let linkContent = "";
            //        if (endPosition > 0) {
            //            let fileName = arr[j].substr(0, endPosition);
            //            let fileNameParts = fileName.split(':');
            //            if (fileNameParts.length == 2) {
            //                linkContent = `[${fileNameParts[0]}](${this.VIRTUAL_ROOT}/${fileNameParts[1]})`;
            //            } else {
            //                linkContent = `[${fileName}](${this.VIRTUAL_ROOT}/${fileName})`;
            //            }
            //        }
            //        if (linkContent == "") {
            //            targetContent += START + arr[j];
            //        } else {
            //            targetContent += linkContent + arr[j].substr(endPosition + END.length);
            //        }
            //    }
            //}
            {
                let a = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>${mdFileName} - Markdown</title>
  <!--header-->${this.MarkdownFileHeader}
</head>
<body>
    `;
                let b = `${this.MarkdownFileEnd}
</body>`
                let htmlContent = a + marked(targetContent) + b;
                res.header('content-type', 'text/html; charset=utf-8');
                res.sendRaw(htmlContent);
            }
            return next();
        };
        this.Server.get('/md/' + ':file(^[^_\\\\\\/\\*\\?\\.]{1}[^\\\\\\/\\*\\?\\.]*).md', mdResponse);
        this.Server.get(this.VIRTUAL_ROOT + '/:file(^[^_\\\\\\/\\*\\?\\.]{1}[^\\\\\\/\\*\\?\\.]*).md', mdResponse);
        this.Server.get('/md/' + ':file(^[^_\\\\\\/\\*\\?\\.]{1}[^\\\\\\/\\*\\?\\.]*)', mdResponse);
        this.Server.get(this.VIRTUAL_ROOT + '/:file(^[^_\\\\\\/\\*\\?\\.]{1}[^\\\\\\/\\*\\?\\.]*)', mdResponse);
        this.Server.get('/wiki/' + ':file(^[^_\\\\\\/\\*\\?\\.]{1}[^\\\\\\/\\*\\?\\.]*).md', mdResponse);
        this.Server.get('/wiki/' + ':file(^[^_\\\\\\/\\*\\?\\.]{1}[^\\\\\\/\\*\\?\\.]*)', mdResponse);
    }

    protected TryExecute(req: restify.Request, res: restify.Response, next, module: string, action: string) {
        let m = null;
        let exMessage: string = "";
        try {
            m = require(module);
        } catch (ex) {
            exMessage = ex.message;
            console.error(CONTENT_EXCEPTION_DB_REST + ex.message);
        }
        if (m) {
            let o = new m.default();
            if (o[action]) {
                return o[action](req, res, next);
            } else {
                var errNoAction = new Error();
                errNoAction.toString = () => { return ACTION_INVALID_ERROR_TITLE_DB_REST + ", " + ACTION_INVALID_ERROR_CONTENT_DB_REST + action; };
                (errNoAction as any).toJSON = () => {
                    return { code: ACTION_INVALID_ERROR_TITLE_DB_REST, message: ACTION_INVALID_ERROR_CONTENT_DB_REST + action };
                };
                res.send(errNoAction);
                next();
            }
        } else {
            var errInvalidModule = new Error();
            errInvalidModule.toString = () => { return MODULE_INVALID_ERROR_TITLE_DB_REST + ", " + MODULE_INVALID_ERROR_CONTENT_DB_REST + exMessage; };
            (errInvalidModule as any).toJSON = () => {
                return { code: MODULE_INVALID_ERROR_TITLE_DB_REST, message: MODULE_INVALID_ERROR_CONTENT_DB_REST + exMessage };
            };
            res.send(errInvalidModule);
            next();
        }
    };
    private ServerGetOrHeadAct(url: string, path: string, version: string, act: string) {
        let params = { url: url, path: path, version: version, act: act };
        this.AllRouters.push(this.Server.get({ path: params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));
        this.AllRouters.push(this.Server.head({ path: params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));

        this.AllRouters.push(this.Server.get({ path: this.VIRTUAL_ROOT + params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));
        this.AllRouters.push(this.Server.head({ path: this.VIRTUAL_ROOT + params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));
    };
    private ServerDelAct(url, path, version, act) {
        let params = { url: url, path: path, version: version, act: act };
        this.AllRouters.push(this.Server.del({ path: params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));

        this.AllRouters.push(this.Server.del({ path: this.VIRTUAL_ROOT + params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));
    };
    private ServerPutAct(url, path, version, act) {
        let params = { url: url, path: path, version: version, act: act };
        this.AllRouters.push(this.Server.put({ path: params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));

        this.AllRouters.push(this.Server.put({ path: this.VIRTUAL_ROOT + params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));
    };
    private ServerPostAct(url, path, version, act) {
        let params = { url: url, path: path, version: version, act: act };
        this.AllRouters.push(this.Server.post({ path: params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));

        this.AllRouters.push(this.Server.post({ path: this.VIRTUAL_ROOT + params.url, version: params.version }
            , (req, res, next) => { return this.TryExecute(req, res, next, params.path, params.act); }));
    };
    private LoadConfigRoutes() {

        $routes.LoadRoute(this.config);

        let groupName: string;
        for (groupName in $routes.objects) {
            ModuleLoader.WatchModuleOfDirectory(this.APP_ROOT + this.SERVICE_ROOT_01 + "/" + groupName);
        }

        let url: string, actConfig: Routes.IRouteItem, path: string, version: string, act: string, key: string;
        for (key in $routes.getsOrHeads) {
            url = key;
            actConfig = $routes.getsOrHeads[key];
            path = actConfig.path;
            act = actConfig.action;
            version = actConfig.version;
            this.ServerGetOrHeadAct(url, path, version, act);
        }

        for (key in $routes.deletes) {
            url = key;
            actConfig = $routes.deletes[key];
            path = actConfig.path;
            act = actConfig.action;
            version = actConfig.version;
            this.ServerDelAct(url, path, version, act);
        }
        for (key in $routes.puts) {
            url = key;
            actConfig = $routes.puts[key];
            path = actConfig.path;
            act = actConfig.action;
            version = actConfig.version;
            this.ServerPutAct(url, path, version, act);
        }
        for (key in $routes.posts) {
            url = key;
            actConfig = $routes.posts[key];
            path = actConfig.path;
            act = actConfig.action;
            version = actConfig.version;
            this.ServerPostAct(url, path, version, act);
        }
    };
    /*为了将restify模块内部方法外露,确保在运行时环境直接依赖组件已下载的依赖,减少开发时*/

    /**
     * 获取静态响应请求通用句柄
     * @param options
     */
    public serveStatic(options?: ServeStatic): restify.RequestHandler {
        return plugins.serveStatic(options);
    }
}