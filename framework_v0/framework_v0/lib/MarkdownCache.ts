import fs = require('fs');
import events = require('events');
import path = require('path');

interface IMarkdownCaches {
    [index: string]: IMarkdownCache;
}

interface IMarkdownCache {
    Load();
    Close();
    Content: string;
}

export default class MarkdownCache extends events.EventEmitter implements IMarkdownCache {
    public static Caches: IMarkdownCaches;
    public static readonly EVENTS_RELOAD = 'reload';
    public static Get(fullPath: string): string {
        fullPath = path.normalize(fullPath);
        let cache = MarkdownCache.Caches[fullPath];
        let cacheContent = cache == null ? "" : cache.Content;
        return cacheContent;
    }
    public static IsExists(fullPath: string): boolean {
        fullPath = path.normalize(fullPath);
        if (MarkdownCache.Caches[fullPath]) { return true; }
        else { return false; }
    }
    public static tryCacheNewTimer: any = null;
    public static tryCacheNew(fullPath: string) {

    }
    private DataFile: string;
    private tryCacheTimer: any = null;
    private fsWatch: fs.FSWatcher = null;
    public Content: string;

    public constructor(fullPath: string) {
        super();
        fullPath = path.normalize(fullPath);
        this.DataFile = fullPath;
    }
    public Load() {
        try {
            this.Content = fs.readFileSync(this.DataFile, "utf-8");
        } catch (ex) {
            console.error("cache file exception!\t" + ex.message);
        }
        if (MarkdownCache.Caches[this.DataFile] !== undefined) {
            let watch: IMarkdownCache = MarkdownCache.Caches[this.DataFile];
            watch.Close();
        }
        MarkdownCache.Caches[this.DataFile] = this;
        var defaultHandler = () => { console.log("Cache OK:\t" + this.DataFile); };
        this.on(MarkdownCache.EVENTS_RELOAD, defaultHandler);
        console.log("Watching markdown file:\t" + this.DataFile);
        this.fsWatch = fs.watch(this.DataFile, (eventType: string, fileName: string) => {
            this.fileChanged(eventType, fileName);
        });
    }

    private fileChanged(eventType: string, fileName: string) {//解除文件变化时快速重复多次事件触发
        if (this.tryCacheTimer !== null) clearTimeout(this.tryCacheTimer);
        this.tryCacheTimer = setTimeout(() => { this.executeReloadCacheAgain(); }, 200);
    }
    private executeReloadCacheAgain() {
        if (this.tryCacheTimer !== null) clearTimeout(this.tryCacheTimer);
        console.log("Reload markdown file:\t" + this.DataFile);
        this.requireReloadAgain();
    }
    private requireReloadAgain() {
        try {
            this.Content = fs.readFileSync(this.DataFile, "utf-8");
        } catch (ex) {
            console.error("cache file exception!\t" + ex.message);
        }
        this.emit(MarkdownCache.EVENTS_RELOAD);
    };
    public Close() {
        console.log("Close markdown file:\t" + this.DataFile);
        this.fsWatch.close();
        delete (MarkdownCache.Caches[this.DataFile]);
    }
}

MarkdownCache.Caches = {};