export interface IAppConfig {
    /**
     * 是否开启服务器端调试模式
     */
    DEBUG_MODE: boolean,
    /**
     * 应用名称,也是restify的服务名称,默认restdb
     */
    APP_NAME: string,
    /**
     * 应用根目录,默认为无效,必须通过__dirname赋值为站点根目录
     */
    APP_ROOT: string
    /**
     * 调试时的监控文件，用于实时刷新客户端
     * 默认值 /public/js/lib.js
     */
    DEBUG_WATCH_FILE:string
}

export interface IServerConfig {
    /**
     * 端口,响应端口
     * 默认 parseInt(process.env.PORT) || 1337
     */
    PORT: number,
    /**
     * 请求路径,虚拟站点路径,主要用于在路径上表达服务分类用途
     * 默认 /restdb
     */
    VIRTUAL_ROOT: string,
    /**
     * 响应HOST名称,或域名
     * 默认 127.0.0.1
     */
    HOST: string
    /**
     * 服务响应模块所在的顶级文件夹
     * 默认 ./restdb
     */
    SERVICE_ROOT_01: string,
    /**
     * 自动寻找模块并自建路由时的默认版本号
     * 默认 1.0.0
     */
    DEFAUTL_VERSION: string,
    /**
     * 自描述响应模块的资源名称
     * 默认 _meta
     */
    META_OBJECT_NAME: string,
    /**
     * 公开响应静态页面及配套静态资源
     * 默认 ./public
     */
    PUBLIC_STATIC_DIRECTORY: string,
    /**
     * restify服务器的默认版本号
     * 默认 1.0.0
     */
    RESTIFY_SERVER_DEFAULT_ROUTE_VERSION: string,
}

const appConfig: IAppConfig = {
    DEBUG_MODE: true,
    APP_NAME: "restdb",
    APP_ROOT: null,
    DEBUG_WATCH_FILE: "/public/js/lib.js"
}
//exports.AppConfig = appConfig;


const serverConfig: IServerConfig = {
    PORT: parseInt(process.env.PORT) || 6379,
    VIRTUAL_ROOT: "/restdb",
    HOST: '127.0.0.1',
    SERVICE_ROOT_01: "/restdb",
    DEFAUTL_VERSION: "1.0.0",
    META_OBJECT_NAME: '_meta',
    PUBLIC_STATIC_DIRECTORY: "/public",
    RESTIFY_SERVER_DEFAULT_ROUTE_VERSION: "1.0.0"
};
//exports.ServerConfig = serverConfig;

//export interface IRedisConfig {
    
//    REDIS_PORT: number,
//    REDIS_HOST: string
//}
//const redisConfig: IRedisConfig = {
//    REDIS_PORT: 6379,
//    REDIS_HOST: '127.0.0.1'
//};
//exports.RedisConfig = redisConfig;

export interface IConfig {
    AppConfig: IAppConfig,
    ServerConfig: IServerConfig
    //RedisConfig: IRedisConfig
}
const Default: IConfig = { AppConfig: appConfig, ServerConfig: serverConfig };
//const Default: IConfig = { AppConfig: appConfig, ServerConfig: serverConfig, RedisConfig: redisConfig };

export default Default;
