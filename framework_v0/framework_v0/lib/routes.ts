import fs = require('fs');
import { IConfig } from "./config";

export interface IRouteItem {
    /**
     * 响应版本
     */
    version: string,
    /**
     * 响应JS模块路径
     */
    path: string,
    /**
     * HTTP请求行为
     */
    action: string
}
export interface IRouteGroup {
    /**
     * 同一资源, 请求HTTP路径与路由完整定义IRouteItem之间的映射关系
     * {
     *     example: ["first"],
     *     simple: []
     * };
     */
    [index: string]: IRouteItem
}
export interface IObjectGroup {
    /**
     * 同一组中,组名称与资源名称集合之间的映射关系
     */
    [index: string]: string[]
}
export interface IRouteConfig {
    /**
     * 根据预设规划自动遍历$serviceRoot下子文件夹,并加载预设的路由关系
     */
    LoadRoute: ILoadRouteFunc,
    /**
     * 组名称的集合,及其与资源名称集合之间的映射关系
     */
    objects: IObjectGroup,
    /**
     * GET or HEAD请求类路由集合
     */
    getsOrHeads: IRouteGroup,
    /*
     * DELETE请求类路由集合
     */
    deletes: IRouteGroup,
    /**
     * PUT请求类路由集合
     */
    puts: IRouteGroup,
    /**
     * POST类请求类路由集合
     */
    posts: IRouteGroup,
}
/**
 * 加载路由的行为函数
 */
export interface ILoadRouteFunc {
    (config: IConfig):void
}
/**
 * 缓存已经加载的组名称的集合,及其与资源名称集合之间的映射关系
 * {
 *     example: ["first"],
 *     simple: []
 * };
 */
export let OBJECTS: IObjectGroup = {};

function SearchObjectOfDirectory(dir: string, config: IConfig){
    let searchDir = dir;
    if (searchDir.lastIndexOf("/") !== searchDir.length - 1) {
        searchDir += "/";
    }
    
    let searchGroup = (searchGroupDir: string) => {
        let dirOfFileNames = fs.readdirSync(searchGroupDir);
        if (!dirOfFileNames) { return; }
        dirOfFileNames.forEach((dirOrfileName) => {
            if (dirOrfileName.indexOf('.') >= 0) {
                return;
            }
            //找到新的组
            let objectGroupName: string = dirOrfileName;
            //查找对象清单
            searchObject(searchGroupDir + dirOrfileName, objectGroupName);
        });
    };
    let searchObject = (searchObjectDir: string, groupName: string) => {
        console.info('GROUP:' + groupName)
        OBJECTS[groupName] = [];
        let dirOfFileNames = fs.readdirSync(searchObjectDir);
        if (!dirOfFileNames) { return; }
        dirOfFileNames.forEach((dirOrfileName) => {
            let searchPosition: number = dirOrfileName.lastIndexOf('.js');
            let endWithJS = /\.js$/.test(dirOrfileName);
            if (endWithJS) {
                let objectName: string = dirOrfileName.substr(0, searchPosition);
                if (config.ServerConfig.META_OBJECT_NAME === objectName) return;
                OBJECTS[groupName].push(objectName);
                console.info('OBJECT:' + objectName)
            }
        });
    }
    searchGroup(searchDir);
};
let LoadRoute = (config: IConfig) => {
    let objectCode: string;
    let objectArray: string[];
    let objectPath: string;
    let serviceRoot = config.AppConfig.APP_ROOT + config.ServerConfig.SERVICE_ROOT_01;
    SearchObjectOfDirectory(serviceRoot, config);

    objectPath = "/" + config.ServerConfig.META_OBJECT_NAME;
    routes.getsOrHeads[objectPath + "/"] = {
        version: config.ServerConfig.DEFAUTL_VERSION,
        path: serviceRoot + objectPath, action: "Read"
    }
    routes.getsOrHeads[objectPath] = {
        version: config.ServerConfig.DEFAUTL_VERSION,
        path: serviceRoot + objectPath, action: "Read"
    }
    for (let groupName in OBJECTS) {
        objectArray = OBJECTS[groupName];
        objectPath = "/" + groupName + "/" + config.ServerConfig.META_OBJECT_NAME;
        routes.getsOrHeads[objectPath] = {
            version: config.ServerConfig.DEFAUTL_VERSION,
            path: serviceRoot + objectPath, action: "Read"
        }
        routes.getsOrHeads[objectPath + "/"] = {
            version: config.ServerConfig.DEFAUTL_VERSION,
            path: serviceRoot + objectPath, action: "Read"
        }
        for (var i in objectArray) {
            objectCode = objectArray[i];
            objectPath = "/" + groupName + "/" + objectCode;
            routes.getsOrHeads[objectPath] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "Read"
            }
            //routes.getsOrHeads["/" + objectCode + "?from=:from&to=:to"] = { version: $config.ServerConfig.DEFAUTL_VERSION, path: $serviceRoot + "/" + objectCode, action: "read" }
            routes.getsOrHeads[objectPath + "/:id"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "ReadByID"
            }
            routes.deletes[objectPath + "/:id"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "DelByID"
            }
            routes.posts[objectPath] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "Insert"
            }
            routes.puts[objectPath + "/:id"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "UpdateByID"
            }

            routes.getsOrHeads[objectPath + "/"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "Read"
            }
            //routes.getsOrHeads["/" + objectCode + "?from=:from&to=:to"] = { version: $config.ServerConfig.DEFAUTL_VERSION, path: $serviceRoot + "/" + objectCode, action: "read" }
            routes.getsOrHeads[objectPath + "/:id/"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "ReadByID"
            }
            routes.deletes[objectPath + "/:id/"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "DelByID"
            }
            routes.posts[objectPath + "/"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "Insert"
            }
            routes.puts[objectPath + "/:id/"] = {
                version: config.ServerConfig.DEFAUTL_VERSION,
                path: serviceRoot + objectPath, action: "UpdateByID"
            }
        }
    }

}

let routes: IRouteConfig = {
    LoadRoute: LoadRoute,
    objects: OBJECTS,
    getsOrHeads: {
        //"/first": { version: $config.ServerConfig.DEFAUTL_VERSION, path: $serviceRoot + "/first", action: "lists"},
        //"/first/:id": { version: $config.ServerConfig.DEFAUTL_VERSION, path: $serviceRoot + "/first", action: "getByID"}
    },
    deletes: {
        //"/first/:id": { version: $config.ServerConfig.DEFAUTL_VERSION, path: $serviceRoot + "/first", action: "delByID"}
    },
    puts: {
        //"/first": { version: $config.ServerConfig.DEFAUTL_VERSION, path: $serviceRoot + "/first", action: "insert"}
    },
    posts: {
        //"/first/:id": { version: $config.ServerConfig.DEFAUTL_VERSION, path: $serviceRoot + "/first", action: "update"}
    }
};
//LoadRoute();

export default routes;
