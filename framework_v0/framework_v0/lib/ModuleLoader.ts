import fs = require("fs");
import path = require("path");
import events = require("events");
import ModuleWatch from "./ModuleWatch";

export default class ModuleLoader extends events.EventEmitter {
    public static WatchModuleOfDirectory(dir: string){
        let watchDir = dir;
        if (watchDir.lastIndexOf("/") !== watchDir.length - 1) {
            watchDir += "/";
        }
        let startWatch = () => {
            let fullPath = "";
            fs.readdir(watchDir, (err, fileNames) => {
                if (!err) {
                    fileNames.forEach(function (fileName) {
                        ModuleLoader.WatchModule(watchDir, fileName);
                    });
                }
            });
            setTimeout(startWatch, 60000);//每隔60秒重新遍历,以检查新是否有新的文件
        };
        startWatch();
    };
    private static IsExists(fullPath: string): boolean {
        fullPath = path.normalize(fullPath);
        if (ModuleWatch.Caches[fullPath]) { return true; }
        else { return false; }
    }
    private static WatchModule(watchDir: string, fileName: string) {
        let endOfJSONOrJS = /\.json$|\.js$/.test(fileName);
        if (endOfJSONOrJS) {
            let fullPath = watchDir + fileName;
            if (!ModuleLoader.IsExists(fullPath)) {
                let moduleWatch = new ModuleWatch(fullPath);
            }
        }
    };

}