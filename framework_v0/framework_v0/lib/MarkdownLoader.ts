import fs = require('fs');
import events = require('events');
import path = require('path');
import MarkdownCache from './MarkdownCache';

export default class MarkdownLoader extends events.EventEmitter {
    public constructor() {
        super();
    }
    public Load(dir: string){
        var watchDir = dir;
        if (watchDir.lastIndexOf("/") !== watchDir.length - 1) {
            watchDir += "/";
        }
        let startWatch = () => {
            let fullPath = "";
            fs.readdir(watchDir, (err, fileNames) => {
                if (!err) {
                    fileNames.forEach((fileName) => {
                        this.LoadAndWatchMarkdown(watchDir, fileName);
                    });
                }
            });
        };
        startWatch();
    }
    private LoadAndWatchMarkdown(watchDir: string, fileName: string) {
        let endOfJSONOrJS = /\.md|\.MD$/.test(fileName);
        if (endOfJSONOrJS) {
            let fullPath = watchDir + fileName;
            fullPath = path.normalize(fullPath);
            if (!MarkdownCache.Caches[fullPath]) {
                let cache = new MarkdownCache(fullPath);
                cache.Load();
            }
        }
    }
}

