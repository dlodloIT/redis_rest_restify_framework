//方便调试时自动监听指定文件的变化来实现页面自动刷新
//需要浏览器配合设置,IE中,需要设置缓存更新 为 每次访问网页时, chrome需要在开发工具Network中勾选Disable Cache
if (document.domain === 'localhost') {
    var socketRootUrl = "http://localhost:8000";
    var socket = io.connect(socketRootUrl);
    socket.on('debugFileChanged', function (data) {
        console.log(data);
        document.location.reload(true);
    });
    socket.on('debugModuleChanged', function (data) {
        console.log(data);
        document.location.reload(true);
    });
}