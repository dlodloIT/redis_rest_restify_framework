import $config, { IConfig } from "./lib/config";
import IServer from "./lib/IServer";
import ServerA from "./lib/ServerA";
import ModuleLoader from "./lib/ModuleLoader";
import ModuleWatch from "./lib/ModuleWatch";
import IModuleWatchCache from "./lib/ModuleWatch";
import RestObjectModule from "./lib/RestObjectModule";

let createServerA = (config: IConfig): IServer => {
    var server = new ServerA(config);
    return server;
}
let createModuleWatch = (file: string): ModuleWatch => {
    let moduleWatch = new ModuleWatch(file);
    return moduleWatch;
}

let watchModuleOfDirectory = (dir: string) => {
    ModuleLoader.WatchModuleOfDirectory(dir);
}

//let getModuleWatchCaches = function (): IModuleWatchCache {
//    return ModuleWatch.Caches;
//}

export = {
    CreateServerA: createServerA,
    Config: $config,
    ModuleWatchCaches: ModuleWatch.Caches,
    EVENTS_RELOAD_ModuleWatch: ModuleWatch.EVENTS_RELOAD,
    CreateModuleWatch: createModuleWatch,
    WatchModuleOfDirectory: watchModuleOfDirectory,
    RestObjectModule: RestObjectModule,
    ServerA: ServerA
}

