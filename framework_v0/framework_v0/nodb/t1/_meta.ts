import ObjectsMeta, { IObjectMetaInfo } from '../../ObjectsMeta';

export default class _Meta extends ObjectsMeta {
    OnInitData() {
        this.objectsData = require('./_metaJSON');
    }
}