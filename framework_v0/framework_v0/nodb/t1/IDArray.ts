import IDArrayHandler, { IDataItem } from '../../IDArrayHandler';

export default class IDArray extends IDArrayHandler {
    OnInitData() {
        this.idArrayData = require('./IDArrayData');
    }
}